// import { BigNumber } from "ethers";
import { DeployFunction } from "hardhat-deploy/types";


const main: DeployFunction = async function({ getNamedAccounts, deployments }) {
    const { deploy } = deployments;
    const { deployer } = await getNamedAccounts();

    await deploy("RemixCollectionFactory", {
        from: deployer,
        args:[],
        log: true,
    });

    await deploy("RemixCollection", {
        from: deployer,
        args: [],
        log: true,
    });
};


export default main;
main.tags = ["Remix", "RemixCollectionFactory"];