import fs from "fs";
import path from "path";

import dotenv from "dotenv";
import { Contract } from "ethers";
import { ethers } from "hardhat";
import { DeployFunction } from "hardhat-deploy/types";
import { create, globSource } from "ipfs-http-client";
import { CID } from "multiformats";
import NsContracts from "types/contracts";
import { sys } from "typescript";

import { remixBuildArgs } from "helpers/tests";
import NsRemix from "types/Remix";
import logger from "utils/logger";

import RemixFactoryArtifact from "../artifacts/contracts/RemixFactory.sol/RemixFactory.json";


// NOTE: Issue between ipfs-http-client (>56) and hardhat as described here:
// https://github.com/ipfs/js-ipfs/issues/4139#issuecomment-1233119810
// ipfs-http-client 56.0.2 is the last version that works with hardhat,
// using that version for now.

// IPFS server is the same used by the app
dotenv.config({
    path: path.resolve(__dirname, "../../app/src/.env")
});

// Get the IPFS server host, port and protocol
const IPFS_SERVER_HOST = "ipfs.infura.io";
const IPFS_SERVER_PORT = 5001;
const IPFS_SERVER_PROTOCOL = "https";
const IPFS_ENDPOINT = process.env.NEXT_PUBLIC_IPFS_ENDPOINT ? process.env.NEXT_PUBLIC_IPFS_ENDPOINT : "https://ipfs.infura.io/ipfs/";
const IPFS_AUTH = "Basic " + Buffer.from(
    process.env.NEXT_PUBLIC_INFURA_ID + ":" + process.env.NEXT_PUBLIC_INFURA_SECRET
).toString("base64");

const ipfs = create({
    host: IPFS_SERVER_HOST,
    port: IPFS_SERVER_PORT,
    protocol: IPFS_SERVER_PROTOCOL,
    headers: { authorization: IPFS_AUTH }
});

/**
 * Uploads the metadata to IPFS.
 * @param collectible The collectible image file name.
 * @param rmx The RMX image file name.
 * @param file The "files" image file name.
 * @param name The name of the collectible.
 * @param description The description of the collectible.
 * @param cid The CID of the directory.
 * @returns The URI of the metadata.
 */
async function uploadMetadata(
    collectible: string,
    rmx: string,
    file: string,
    name: string,
    description: string,
    cid: CID
) {
    const collectibleMetadata = {
        name: name,
        description: description,
        image: IPFS_ENDPOINT + cid.toString() + "/" + collectible
    };

    const RMXMetadata = {
        name: name + " - RMX",
        description: "RMX: " + description,
        image: IPFS_ENDPOINT + cid.toString() + "/" + rmx,
        file: IPFS_ENDPOINT + cid.toString() + "/" + file
    };

    const files = [];

    files.push({
        path: "metadata/0.json",
        content: JSON.stringify(collectibleMetadata)
    });

    files.push({
        path: "metadata/1.json",
        content: JSON.stringify(RMXMetadata)
    });

    let hash = "";
    for await (const result of ipfs.addAll(files)) {
        if (result.path === "metadata") {
            hash = result.cid.toString();
        }
    }

    const uri = IPFS_ENDPOINT + hash + "/{id}.json";

    return uri;
}

/**
 * Deploys a Remix from the RemixFactory.
 * @param factory The RemixFactory contract.
 * @param args The Remix build arguments.
 * @returns The address of the deployed Remix proxy.
 */
async function deployRemixFromFactory(
    factory: Contract,
    args: NsRemix.IsContractBuildArgs
): Promise<string> {
    // Deploy the Remix
    const tx = await factory.deploy(args);
    const receipt = await tx.wait();

    // Get the RemixDeployed event
    const event = receipt.events?.find((e: { event: string; }) => e.event === "RemixDeployed");

    // Get the Remix address
    const remixProxyAddr = event?.args?.remixProxyAddress;

    return remixProxyAddr;
}

/**
 * Deploys the Remixes.
 */
const main: DeployFunction = async function ({ getNamedAccounts }) {
    console.log("\n================== EXAMPLE REMIXES DEPLOYMENT ==================");

    // const { deploy } = deployments;
    const { deployer } = await getNamedAccounts();
    const burner = "0xE8B791bF71366717D1B0bDe618CaD05d91448798";

    logger.info("Uploading metadata..");

    // First, we upload the metadata to IPFS and get the CID
    const tokenUris: NsContracts.IsMetadata[] = [
        {
            collectible: "01_collectible.gif",
            rmx: "01_RMX.gif",
            file: "01_files.gif",
            name: "BrokenCubes",
            description: "A work on destructuring structures.",
            result: "",
            cid: null
        },
        {
            collectible: "02_collectible.png",
            rmx: "02_RMX.png",
            file: "02_files.zip",
            name: "PWords",
            description: "Collaborative play on words starting with P, rendered as WordCloud.",
            result: "",
            cid: null
        },
        {
            collectible: "03_collectible.png",
            rmx: "03_RMX.png",
            file: "03_files.svg",
            name: "PWords framed",
            description: "A framed version of PWords.",
            result: "",
            cid: null
        }
    ];

    let UrisIndex = 0;
    const assets = fs.readdirSync("./assets");

    // Uploading the metadata and getting the CID of each asset
    for (const asset of assets) {
        logger.info(`Uploading asset: '${asset}'..`);

        const assetPath = path.resolve("./assets", asset);

        for await (const file of ipfs.addAll(globSource(assetPath, "**/*"))) {
            tokenUris[UrisIndex].cid = file.cid;
        }

        tokenUris[UrisIndex].result = await uploadMetadata(
            tokenUris[UrisIndex].collectible,
            tokenUris[UrisIndex].rmx,
            tokenUris[UrisIndex].file,
            tokenUris[UrisIndex].name,
            tokenUris[UrisIndex].description,
            tokenUris[UrisIndex].cid
        );

        UrisIndex++;
    }

    logger.info("Uploading done!\n");

    // Dynamic import of RemixFactory contract
    const ganacheDeploymentPath = path.join(__dirname, "../deployments/ganache/RemixFactory.json");
    const localhostDeploymentPath = path.join(__dirname, "../deployments/localhost/RemixFactory.json");

    let finalPath = "";
    if (fs.existsSync(ganacheDeploymentPath)) {
        finalPath = ganacheDeploymentPath;
    } else if (fs.existsSync(localhostDeploymentPath)) {
        finalPath = localhostDeploymentPath;
    } else {
        logger.error(`RemixFactory deployment not found! Please check\n${finalPath}`);
        sys.exit(1);
    }

    // Deployment
    logger.info("Artifact found!\n");

    logger.info("Deployment..");

    // Deploys three remix token to populate
    let args: NsRemix.IsContractBuildArgs;

    const remixFactoryDeployment = await import(finalPath);
    const remixFactory = await ethers.getContractAtFromArtifact(RemixFactoryArtifact, remixFactoryDeployment.address);

    // Deploy Remix 1 with no child
    logger.info("Deploying Remix 001 contract..");
    args = remixBuildArgs([deployer], [], "");  // tokenUris[0].result
    const remix1Addr = await deployRemixFromFactory(remixFactory, args);
    logger.info(`Remix 001 deployed at: ${remix1Addr}`);

    // Deploy Remix 2 with no child and author is the default burner waller
    logger.info("Deploying Remix 002 contract..");
    args = remixBuildArgs([deployer, burner], [], "");  // tokenUris[1].result
    const remix2Addr = await deployRemixFromFactory(remixFactory, args);
    logger.info(`Remix 002 deployed at: ${remix2Addr}`);

    // Deploy Remix 3 with Remix 2 as parent
    // logger.info("Deploying Remix 003 contract..");
    // args = remixBuildArgs([deployer], [remix2Addr], "");  // tokenUris[2].result
    // const remix3Addr = await deployRemixFromFactory(remixFactory, args);
    // logger.info(`>> Remix 003 deployed at: ${remix3Addr}`);

    logger.info("Deployments done!");
};


export default main;
main.tags = ["Examples"];
main.dependencies = ["RemixFactory"];