import { BigNumber } from "ethers";
import { DeployFunction } from "hardhat-deploy/types";


const main: DeployFunction = async function({ getNamedAccounts, deployments }) {
    const { deploy } = deployments;
    const { deployer } = await getNamedAccounts();

    await deploy("RemixFactory", {
        from: deployer,
        args:[BigNumber.from(500)],
        log: true,
    });

    await deploy("Remix", {
        from: deployer,
        args: [],
        log: true,
    });
};


export default main;
main.tags = ["Remix", "RemixFactory"];