import NsContracts from "types/contracts";


/**
 * A list of general configuration options.
 */
export const GENERAL: NsContracts.IsConfigGENERAL = {
    xdaiSleepingTime: 1000,

    // Winston logger verbosity
    verbose: false,

    // Address used by the minting script
    mintingAddress: "0x8760Db2223686B352D4993DEb77A47982C502992",

    // Contains all the recorded contracts and their corresponding MongoDB collections
    recordedContracts: {
        "RemixFactory": "factories",
        "RemixCollectionFactory": "collectionFactories"
    },

    // As a reminder, contract addresses are recorded in the MongoDB database
    // this list allows you to add networks where the contracts are ignored
    ignoredNetworks: [
        "ganache",
        "localhost"
    ]
};

/**
 * A list of paths used by the multiple script.
 */
export const PATHS = {
    // The path going to the automatically generated types by Hardhat/TypeChain
    typesInput: "typechain-types",

    // The path leading to the contract artifacts
    // Copy the artifacts to the different app packages
    artifactsInput: "artifacts/contracts",

    // A list of output paths to copy the artifacts to
    outputs: [
        "../app/src/services/remix/contracts",
        "../remixsubgraphs/remixsubgraph-mumbai/abis",
        "../remixsubgraphs/remixcollectionsubgraph-mumbai/abis"
    ]
};