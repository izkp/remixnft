// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's Ownable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol
 *
 * Using OpenZeppelin's ProxyAdmin.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/proxy/transparent/ProxyAdmin.sol
 *
 * Using OpenZeppelin's AccessControl.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/AccessControl.sol
 *
 * Using OpenZeppelin's ERC165Storage.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/introspection/ERC165Storage.sol
 */

import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Storage.sol";
import "./proxies/RemixCollectionProxy.sol";
import "./RemixCollection.sol";


/// @title Interface for Remix Collection Factory
interface IRemixCollectionFactory {
    /// @notice Get the RemixCollection implementation address
    function getRemixCollectionImplementation() external view returns (address _remixCollectionImplementation);
}


/// @title Main Remix Collection Factory contract
contract RemixCollectionFactory is
    IRemixCollectionFactory,
    Ownable,
    AccessControl,
    ERC165Storage,
    ProxyAdmin
{
    // Roles
    bytes32 public constant MODERATOR_ROLE = keccak256("MODERATOR_ROLE");

    // Addresses
	address private remixCollectionImplementation;
	uint256 public count;

	// Mappings
	mapping(address => address[]) registry;

    // Role events
    event ModeratorAdded(address account);
    event ModeratorRevoked(address account);

    // Events
    event Deployed(address remixCollectionImplementation);
    event RemixCollectionDeployed(address sender, address remixCollectionProxyAddress, address remixCollectionImplementation);
    event RemixCollectionRegistered(address remixCollectionProxyAddress);
    event RemixCollectionImplementationUpgraded(address prevRemixCollectionImplementation, address newRemixCollectionImplementation);

    // Modifiers
    modifier onlyStaff() {
        require(
            msg.sender == owner() ||
            hasRole(MODERATOR_ROLE, msg.sender),
            "NOT_MEMBER_OF_STAFF"
        );

        _;
    }


    constructor() {
        // Deploy RemixCollection implementation contract
        remixCollectionImplementation = address(new RemixCollection());

        // Emit event first for subgraph
        emit Deployed(remixCollectionImplementation);

        // Transfer the ownership
        _transferOwnership(msg.sender);

        // Set up roles
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setRoleAdmin(MODERATOR_ROLE, DEFAULT_ADMIN_ROLE);

        // Register interfaces
        _registerInterface(type(IRemixCollectionFactory).interfaceId);
    }


    /// @dev Deploys a RemixCollection contract and registers it
    /// @param _args Struct containing RemixCollection initializer arguments (prevent stack too deep error)
    /// @return _remixCollectionProxyAddress Address of deployed RemixCollection proxy contract
    function deploy(
        IRemixCollection.deployArgs memory _args
    ) external returns (address _remixCollectionProxyAddress) {
        // Deploy RemixCollection proxy contract
        // Note: Initialize selector cannot be used because of "Stack too deep" error
        RemixCollectionProxy _proxy = new RemixCollectionProxy(
            address(this),
            remixCollectionImplementation,
            address(new ProxyAdmin()),
            abi.encodeWithSelector(RemixCollection.initialize.selector, _args)
        );

        // Get RemixCollection proxy address and register it
        _remixCollectionProxyAddress = address(_proxy);
        registerRemixCollection(_remixCollectionProxyAddress);

        emit RemixCollectionDeployed(msg.sender, _remixCollectionProxyAddress, remixCollectionImplementation);

        return _remixCollectionProxyAddress;
    }

    /// @dev Add a RemixCollection (proxy) to the registry
    /// @param _remixCollectionProxyAddress Address of the RemixCollection proxy contract
    function registerRemixCollection(address _remixCollectionProxyAddress) public {
        // Verifies that the address is a RemixCollection contract
        require(
            IRemixCollection(payable(_remixCollectionProxyAddress)).supportsInterface(type(IRemixCollection).interfaceId),
            "INVALID_REMIX_COLLECTION_CONTRACT"
        );

        registry[msg.sender].push(_remixCollectionProxyAddress);

        count++;

        emit RemixCollectionRegistered(_remixCollectionProxyAddress);
    }

    /// @dev Upgrade RemixCollection implementation contract and all RemixCollection proxy contracts
    ///      using an external RemixCollection implementation contract address
    /// @param _newRemixCollectionImplementation Address of new RemixCollection implementation contract
    function upgradeRemixCollectionImplementation(address _newRemixCollectionImplementation) public onlyOwner {
        require(_newRemixCollectionImplementation != address(0), "NEW_IMPLEMENTATION_ADDRESS_CANNOT_BE_ZERO");
        require(_newRemixCollectionImplementation != remixCollectionImplementation, "IMPLEMENTATION_ADDRESSES_CANNOT_BE_SAME");

        // Save the previous RemixCollection implementation address for event
        address _prevCollectionImplementation = remixCollectionImplementation;

        // Change RemixCollection implementation address
        remixCollectionImplementation = _newRemixCollectionImplementation;

        emit RemixCollectionImplementationUpgraded(_prevCollectionImplementation, _newRemixCollectionImplementation);
    }


    /***************************************
        External & public view interfaces
    ****************************************/

    /// @dev Get the RemixCollection implementation address
    /// @return _remixCollectionImplementation RemixCollection implementation address
    function getRemixCollectionImplementation() external override view returns (address _remixCollectionImplementation) {
        return remixCollectionImplementation;
    }

    /// @dev Get RemixCollection by author
    /// @param _author Address of the author
    /// @return _remixCollectionProxies List of Remix Collections created by the author
    function getRemixCollectionByAuthor(address _author) public view returns (address[] memory _remixCollectionProxies) {
        return registry[_author];
    }

    /// @dev Interface support
    /// @param _interfaceId The interface to check for support
    /// @return _isSupported if the interface is supported
    function supportsInterface(bytes4 _interfaceId)
        public
        view
        override (
            ERC165Storage,
            AccessControl
        )
        returns (bool _isSupported)
    {
        return _interfaceId == type(IRemixCollectionFactory).interfaceId || super.supportsInterface(_interfaceId);
    }


    /*****************************
        Moderator & Staff utils
    ******************************/

    /// @dev Add or revoke moderators
    /// @param _account Address of the moderator
    function addOrRevokeModerator(address _account) public onlyOwner {
        if (!hasRole(MODERATOR_ROLE, _account)) {
            grantRole(MODERATOR_ROLE, _account);

            emit ModeratorAdded(_account);
        } else {
            revokeRole(MODERATOR_ROLE, _account);

            emit ModeratorRevoked(_account);
        }
    }

    /// @dev Check if an address has moderator role
    /// @param _account Address to check
    /// @return _hasModeratorRole True if address has moderator role
    function hasModeratorRole(address _account) public view returns (bool _hasModeratorRole) {
        return hasRole(MODERATOR_ROLE, _account);
    }
}