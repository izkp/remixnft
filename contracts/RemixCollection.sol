// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's OwnableUpgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/access/OwnableUpgradeable.sol
 *
 * Using OpenZeppelin's Initializable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/proxy/utils/Initializable.sol
 *
 * Using OpenZeppelin's IERC20Upgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/token/ERC20/IERC20Upgradeable.sol
 *
 * Using OpenZeppelin's ERC165StorageUpgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/utils/introspection/ERC165StorageUpgradeable.sol
 */

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165StorageUpgradeable.sol";
import { IRemixCollectionFactory } from "./RemixCollectionFactory.sol";
import { IRemix } from "./Remix.sol";


/// @title Interface for Remix Collection
interface IRemixCollection {
    /// @notice Struct for the initialization of Remix RemixCollection
    struct deployArgs {
        string metadata;                                // The Metadata for the RemixCollection
        uint256 submissionPrice;                        // Minimal price to pay to submit a work
        uint256 totalSelections;                        // Number of selection slots available
        address[] startingRemixes;                      // Addresses of Remix contracts that start the RemixCollection
    }

    /// @notice Interface support
    function supportsInterface(bytes4 _interfaceId) external view returns (bool _isSupported);
}

/// @title Main Remix Collection contract
contract RemixCollection is
    IRemixCollection,
    OwnableUpgradeable,
    ERC165StorageUpgradeable
{
    // Structs
    struct indexedRemix {
        address _remixProxyAddress;                      // Address of the Remix proxy contract
        uint256 _selectionNumber;                        // Number of the selection slot
    }

    // Addresses
    address public factory;                             // Factory address
    address[] public allSubmissions;                    // List of all submissions
    address[] public allSelections;                     // List of all selections
    address[] public startingRemixes;                   // Addresses of Remix contracts that start the RemixCollection

    // Strings
    string public metadata;                             // The Metadata for the RemixCollection

    // Uint256s
    uint256 public submissionPrice;                     // Minimal price to pay to submit a work
    uint256 public totalSelections;                     // Number of selection slots available
    uint256 public totalValueHeld;                      // Total value held by the contract

    // Booleans
    bool private finalized;                             // Whether the RemixCollection has been finalized

    // Mappings
    mapping(address => uint256) public submissions;     // Mapping of submissions to their index in allSubmissions
    mapping(uint256 => address) public selections;      // Mapping of selection slots to their address

    // Enums
    enum states {
        Open,
        Closed,
        Finalized
    }

    // Interface IDs
    bytes4 private constant _INTERFACE_ID_RMXCF = type(IRemixCollectionFactory).interfaceId;

    // Status of the RemixCollection (Open, Closed, Finalized)
    states public status;

    // Events
    event Deployed();
    event Opened();
    event Closed();
    event Finalized();
    event RemixSubmitted(address indexed remixProxyAddress, uint256 selectionNumber);
    event RoyaltyReceived(address from, uint256 amount);

    // Modifiers
    modifier isClosed() {
        require(status == states.Closed, "COLLECTION_NOT_CLOSED");
        _;
    }

    modifier isOpen() {
        require(status == states.Open, "COLLECTION_NOT_OPEN");
        _;
    }

    modifier isNotFinalized() {
        require(!finalized, "COLLECTION_IS_FINALIZED");
        _;
    }


    // Empty constructor -> use initialize() instead
    constructor() {
        // Register interfaces
        _registerInterface(type(IRemixCollection).interfaceId);
    }


    /// @dev Initializer for the RemixCollection contract
    /// @param _args Struct containing RemixCollection initializer arguments (prevent stack too deep error)
    function initialize(
        deployArgs memory _args
    ) public initializer {
        require(_args.submissionPrice > 0, "SUBMISSION_PRICE_MUST_BE_GREATER_THAN_ZERO");
        require(_args.totalSelections > 0, "TOTAL_SELECTIONS_MUST_BE_GREATER_THAN_ZERO");

        metadata = _args.metadata;
        submissionPrice = _args.submissionPrice;
        totalSelections = _args.totalSelections;
        startingRemixes = _args.startingRemixes;

        // Change the status to Open
        status = states.Open;

        // Transfer the RemixCollection ownership to the factory
        _transferOwnership(msg.sender);

        emit Deployed();
    }

    /// @dev Open the RemixCollection
    function open() public onlyOwner isNotFinalized {
        require(status != states.Open, "COLLECTION_ALREADY_OPEN");

        status = states.Open;
        emit Opened();
    }

    /// @dev Close the RemixCollection
    function close() public onlyOwner isNotFinalized {
        require(status != states.Closed, "COLLECTION_ALREADY_CLOSED");

        status = states.Closed;
        emit Closed();
    }

    /// @dev Submit a Remix to the RemixCollection
    /// @param _remixProxyAddress Address of the Remix proxy contract
    /// @param _selectionNumber Number of the selection slot
    function submit(address _remixProxyAddress, uint256 _selectionNumber) public payable isNotFinalized isOpen {
        // Verifies that the address is a Remix contract
        require(
            IRemix(payable(_remixProxyAddress)).supportsInterface(type(IRemix).interfaceId),
            "INVALID_REMIX_CONTRACT"
        );

        require(submissions[_remixProxyAddress] == 0, "REMIX_ALREADY_SUBMITTED");
        require(_selectionNumber <= totalSelections, "SELECTION_SLOT_DOES_NOT_EXIST");
        require(_selectionNumber > 0, "SELECTION_SLOT_MUST_BE_GREATER_THAN_ZERO");

        // Must send at least min purchase price
        require(msg.value >= submissionPrice, "INVALID_SUBMISSION_PRICE");

        totalValueHeld += msg.value;
        allSubmissions.push(_remixProxyAddress);
        submissions[_remixProxyAddress] = _selectionNumber;

        emit RemixSubmitted(_remixProxyAddress, _selectionNumber);
    }

    /// @dev Select a Remix to the RemixCollection
    /// @param _remixProxyAddress Address of the Remix proxy contract
    function select(address _remixProxyAddress) public onlyOwner isNotFinalized {
        require(submissions[_remixProxyAddress] != 0, "REMIX_NOT_SUBMITTED");

        uint256 _slot = submissions[_remixProxyAddress];
        selections[_slot] = _remixProxyAddress;
        allSelections.push(_remixProxyAddress);
    }

    /// @dev Finalize the RemixCollection and send the split to the winners
    /// @param _tokenAddress 0 address if ETH, non-0 address if ERC20
    function finalize(address _tokenAddress) public onlyOwner isNotFinalized isClosed {
        require(allSelections.length == totalSelections, "NOT_ALL_SLOTS_HAVE_BEEN_FILLED");

        uint256 _split = totalValueHeld / totalSelections;

        if (_tokenAddress == address(0)) {
            for (uint256 i = 0; i < allSelections.length; i++) {
                (bool _sent, ) = allSelections[i].call{ value: _split }("");
                require(_sent, "ETH_SPLIT_TRANSFER_FAILED");
            }
        } else {
            for (uint256 i = 0; i < allSelections.length; i++) {
                require(
                    IERC20Upgradeable(_tokenAddress).transfer(factory, _split),
                    "ERC20_SPLIT_TRANSFER_FAILED"
                );
            }
        }

        status = states.Finalized;

        // Ensure that the RemixCollection is finalized and cannot be finalized again
        finalized = true;

        emit Finalized();
    }

    /// @dev Emit an event when ETH is received
    receive() external payable {
        emit RoyaltyReceived(msg.sender, msg.value);
    }


    /***************************************
        External & public view interfaces
    ****************************************/

    /// @dev Get the winning Remixes
    /// @notice Returns an empty array if the RemixCollection is not finalized
    /// @return _winningRemixes Array of winning Remixes
    function getWinningRemixes() public view returns (indexedRemix[] memory _winningRemixes) {
        _winningRemixes = new indexedRemix[](totalSelections);

        for (uint256 i = 0; i < totalSelections; i++) {
            _winningRemixes[i] = indexedRemix({
                    _remixProxyAddress: selections[i + 1],
                    _selectionNumber: i + 1
            });
        }

        if (finalized) {
            return _winningRemixes;
        }

        return new indexedRemix[](0);
    }

    /// @dev Get the number of submissions
    /// @return _submissionsCount Number of submissions
    function getSubmissionsCount() public view returns (uint256 _submissionsCount) {
        return allSubmissions.length;
    }

    /// @dev Get the number of selections
    /// @return _selectionsCount Number of selections
    function getSelectionsCount() public view returns (uint256 _selectionsCount) {
        return allSelections.length;
    }

    /// @dev Get the current status of the RemixCollection
    /// @return _status The current status of the RemixCollection
    function getStatus() public view returns (states _status) {
        return status;
    }

    /// @dev Check if a Remix has been submitted
    /// @param _remixProxyAddress Address of the Remix contract
    /// @return _isSubmittedStatus True if the Remix has been submitted
    function isSubmitted(address _remixProxyAddress) public view returns (bool _isSubmittedStatus) {
        if (submissions[_remixProxyAddress] > 0) {
            return true;
        }

        return false;
    }

    /// @dev Interface support
    /// @param _interfaceId The interface to check for support
    /// @return _isSupported if the interface is supported
    function supportsInterface(bytes4 _interfaceId)
        public
        view
        override (
            ERC165StorageUpgradeable,
            IRemixCollection
        )
        returns (bool _isSupported)
    {
        return _interfaceId == type(IRemixCollection).interfaceId || super.supportsInterface(_interfaceId);
    }
}
