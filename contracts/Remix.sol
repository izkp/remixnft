// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's ERC1155SupplyUpgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol
 *
 * Using OpenZeppelin's ERC1155HolderUpgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/token/ERC1155/utils/ERC1155HolderUpgradeable.sol
 *
 * Using OpenZeppelin's IERC20Upgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/token/ERC20/IERC20Upgradeable.sol
 *
 * Using OpenZeppelin's ERC165StorageUpgradeable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts-upgradeable/blob/master/contracts/utils/introspection/ERC165StorageUpgradeable.sol
 */

import "hardhat/console.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155SupplyUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/utils/ERC1155HolderUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/introspection/ERC165StorageUpgradeable.sol";
import { IRemixFactory } from "./RemixFactory.sol";


/// @title Interface for Remix
interface IRemix {
    /// @notice Struct for the initialization of Remix
    struct deployArgs {
        string uri;                     // String to prepend to token IDs
        address[] authors;              // Addresses of creative work authors
        uint256[] authorSplits;         // BPs to share with authors
        address[] parents;              // Addresses of creative work parent contracts
        uint256[] parentSplits;         // BPs to share with parents
        uint256 startingPrice;          // Starting perpetual auction price of RMX token
        uint256 increasePoints;         // BPs to increase RMX token price each time
        uint256 collectiblePrice;       // Price of collectible token
        uint256 usagePrice;             // Price of usage token
        uint256 rmxCountdown;           // How long holder has to create deriviative work after losing RMX token
        uint256 royalties;              // Base royalties for perpetual auction and secondary market
    }

    function authors(uint256) external view returns (address);

    /// @notice Harvest royalties and perform one generation of splits, also takes factory cut
    function harvestRoyalties(address _tokenAddress) external;

    /// @notice Request derivative by a child token contract
    function requestDerivative(address[] memory _authors, address _dst) external returns (bool _derivativeSuccess);

    /// @notice Send a flag token to a child Remix in case of unfair use or other complaints
    function flag(address[] memory _parents) external;

    /// @notice Removes a flag token from a child Remix in case of unfair use or other complaints
    function unflag(address[] memory _parents, uint256 _index) external;

    /// @notice Get the authors only
    function getAuthors() external view returns (address[] memory _authors);

    /// @notice Get the length of the authors array
    function getAuthorsLength() external view returns (uint256 _authorsLength);

    /// @notice Get an author from the authors array
    function getAuthor(uint256 _index) external view returns (address _author);

    /// @notice Interface support
    function supportsInterface(bytes4 _interfaceId) external view returns (bool _isSupported);
}

/// @title Interface for the NFT Royalty Standard
///        Noted as upgradeable for standardization
interface IERC2981Upgradeable is IERC165Upgradeable {
    /// @notice Called with the sale price to determine how much royalty is owed and to whom.
    /// @param _tokenId The NFT asset queried for royalty information
    /// @param _salePrice The sale price of the NFT asset specified by _tokenId
    /// @return _receiver Address of who should be sent the payment
    /// @return _royaltyAmount The royalty payment amount for _salePrice
    function royaltyInfo(uint256 _tokenId, uint256 _salePrice)
        external
        view
        returns (address _receiver, uint256 _royaltyAmount);
}

/// @title Main Remix contract
contract Remix is
    IRemix,
    ERC1155SupplyUpgradeable,
    ERC1155HolderUpgradeable,
    IERC2981Upgradeable,
    ERC165StorageUpgradeable
{
    // Addresses
    address public factory;                                     // Factory address
    address public currentCollectibleOwner;                     // Current collectible owner
    address public currentRMXOwner;                             // An artificial representation of a token
    address[] public override authors;                          // Remix authors
    address[] public parents;                                   // Remix parents
    address[] public flaggingParents;                           // Flagging parents
    address[] public splitAddresses;                            // Receive splits including parent contracts and authors

    // Uint256s
    uint256 public collectiblePrice;                            // Price of collectible token
    uint256 public usagePrice;                                  // Price of usage token
    uint256 public minPurchasePrice;                            // Next lowest price that rmx token can be purchased for
    uint256 public royalties;                                   // Royalties for secondary sales of tokens
    uint256 public increasePoints;                              // BP to increment RMX token price by
    uint256 public countdownTime;                               // Time after recieving badge that holder can mint derivative
    uint256 public factoryCut;                                  // BPs to share with the factory
    uint256[] public authorSplits;                              // Remix authors splits
    uint256[] public parentSplits;                              // Remix parents splits

    // Booleans
    bool argsPassed;                                            // Args passed flag
    bool deployed;                                              // Deployed flag
    bool public hasBeenPurchased;                               // Track if RMX token has been purchased once

    // Mappings
    mapping(address => bool) public flaggingParentsExists;      // Does flagging parent exist
    mapping(address => uint256) canMintUntil;                   // Track how long license is valid for per holder
    mapping(address => uint256) public splits;                  // Amount of splits to send

    // Enums
    enum TokenTypes {
        Collectible,
        RMX,
        Derivative,
        Usage,
        Badge,
        Flag
    }

    // Interface IDs
    bytes4 private constant _INTERFACE_ID_ERC2981 = 0x2a55205a;

    // Events
    event RMXCountdownStarted(address buyer, uint256 expiration);
    event RMXPurchased(address holder, uint256 amount);
    event CollectiblePurchased(address buyer, uint256 amount);
    event UsagePurchased(address buyer, uint256 amount);
    event RoyaltiesHarvested(address[] recipients, address tokenAddress, uint256[] amounts);
    event DerivativeIssued(address dst);
    event Deployed(address[] authors, bool success);
    event BadgeIssued(uint256 tokenId, address dst);
    event RoyaltyReceived(address from, uint256 amount);
    event ParentAdded(address parent);
    event Mint(address dst, uint256 tokenID);
    event Flagged(address by, address remixProxyAddress);
    event UnFlagged(address by, address remixProxyAddress);

    // Modifiers
    modifier isDeployed() {
        require(deployed, "CONTRACT_NOT_DEPLOYED");
        _;
    }

    modifier isAuthor() {
        bool _author = false;

        for (uint256 _i; _i < authors.length; _i++) {
            if (authors[_i] == msg.sender) {
                _author = true;
            }
        }

        require(_author, "SENDER_IS_NOT_AN_AUTHOR");
        _;
    }


    // Empty constructor -> use initialize() instead
    constructor() {
        // Register interfaces
        _registerInterface(_INTERFACE_ID_ERC2981);
        _registerInterface(type(IRemix).interfaceId);
    }


    /// @dev Initializer sets the token base URI, and external interfaces
    /// @param _args Struct containing Remix initializer arguments (prevent stack too deep error)
    function initialize(
        deployArgs memory _args
    ) public initializer {
        require(!argsPassed, "ARGS_ALREADY_PASSED");
        require(_args.authors.length > 0, "MISSING_AUTHORS");
        require(_args.authors.length == _args.authorSplits.length, "AUTHORS_LEN_NOT_EQUAL_TO_AUTHOR_SPLITS_LEN");
        require(_args.parents.length == _args.parentSplits.length, "PARENTS_LEN_NOT_EQUAL_TO_PARENT_SPLITS_LEN");
        require(_args.royalties < 10000, "EXCEEDS_MAX_ROYALTIES");

        // Set the factory address
        // by verifying that the sender is a factory
        bool _isFactory = _verifyFactory(msg.sender);

        if (_isFactory) {
            factory = msg.sender;
        } else {
            factory = address(0);
        }

        // Assing ERC1155 base URI
        __ERC1155_init(_args.uri);
        _setURI(_args.uri);

        // Assign authors and parents
        authors = _args.authors;
        authorSplits = _args.authorSplits;
        parents = _args.parents;
        parentSplits = _args.parentSplits;

        // Initialize split sum to check that sum of all splits is 10000
        bool _senderIsAuthor = _splitSum();

        // Set initial values
        collectiblePrice = _args.collectiblePrice;       // Set collectible price
        usagePrice = _args.usagePrice;                   // Set usage price
        countdownTime = _args.rmxCountdown;              // Set RMX countdown time
        royalties = _args.royalties;                     // Set royalties
        minPurchasePrice = _args.startingPrice;          // First purchase price - active immediately
        increasePoints = _args.increasePoints;           // Set the amount to increase

        if (_senderIsAuthor) {
            currentRMXOwner = msg.sender;
        } else {
            currentRMXOwner = _args.authors[0];
        }

        deployed = true;
        emit Deployed(_args.authors, true);
    }

    /// @dev Purchase Remix token on perpetual auction
    function purchaseRMX() public payable isDeployed {
        require(msg.value >= minPurchasePrice, "INVALID_RMX_PURCHASE_PRICE");
        require(msg.sender != currentRMXOwner, "RMX_BUYER_IS_ALREADY_OWNER");

        // Increment the next purchase price
        minPurchasePrice = (minPurchasePrice * (10000 + increasePoints)) / 10000;

        if (hasBeenPurchased) {
            // If token is being purchased first time keep ETH in contract and distribute via harvest
            uint256 _toSend = (msg.value * (10000 - royalties)) / 10000;  // Send amount to owner less royalties
            (bool _success, ) = currentRMXOwner.call{ value: _toSend }("");

            require(_success, "COULD_NOT_SEND_TO_RMX_OWNER");  // TODO_ONE_DAY: WETH fallback?
        } else {
            hasBeenPurchased = true;  // Set purchased so we skip this next time
        }

        canMintUntil[currentRMXOwner] = block.timestamp + countdownTime;  // Start countdown for previous owner
        currentRMXOwner = msg.sender;

        _mintBadge(msg.sender);  // Mint a badge to new owner

        emit RMXPurchased(msg.sender, msg.value);
    }

    /// @dev Purchase collectible token from initial creator
    function purchaseCollectible() external payable isDeployed {
        // Only allow 1 to be minted parameter
        require(
            totalSupply(uint256(TokenTypes.Collectible)) == 0,
            "COLLECTIBLE_ALREADY_MINTED_OR_PURCHASED"
        );

        // Must send at least the min value
        require(
            msg.value >= collectiblePrice,
            "INVALID_COLLECTIBLE_PURCHASE_PRICE"
        );

        _mintCollectible(msg.sender);
        currentCollectibleOwner = msg.sender;

        emit CollectiblePurchased(msg.sender, msg.value);
    }

    /// @dev Purchase collectible token from initial creator
    function purchaseUsage() external payable {
        require(msg.value >= usagePrice, "INVALID_USAGE_PURCHASE_PRICE");
        require(balanceOf(msg.sender, uint256(TokenTypes.Usage)) < 1, "ALREADY_PURCHASED_USAGE_RIGHTS");

        _mintUsage(msg.sender);

        emit UsagePurchased(msg.sender, msg.value);
    }

    /// @dev Harvest royalties and perform one generation of splits, also takes factory cut
    /// @param _tokenAddress 0 address if ETH, non-0 address if ERC20
    function harvestRoyalties(address _tokenAddress) public override isDeployed {
        // Get the factory cut dinamically from the factory
        _getFactoryCut();

        if (_tokenAddress == address(0)) {
            // Take factory cut
            uint256 _remaining = _takeCut(
                address(this).balance
            );

            // Harvest ETH
            _performSplit(_remaining);
        } else {
            // Take factory cut
            uint256 _remaining = _takeCut(
                _tokenAddress,
                IERC20Upgradeable(_tokenAddress).balanceOf(address(this))
            );

            // Harvest ERC20
            _performSplit(_tokenAddress, _remaining);
        }
    }

    /// @dev Request derivative by a child token contract
    /// @param _authors Address of authors to check for validity of holding a RMX
    /// @param _dst Address of the Remix contract to send the derivative token to
    /// @return _derivativeSuccess Success for the request
    function requestDerivative(address[] memory _authors, address _dst) external override isDeployed returns (bool _derivativeSuccess) {
        // TODO: Isaac

        // Verifies that the address is a Remix contract
        // require(
        //     IERC165Upgradeable(_dst).supportsInterface(type(IRemix).interfaceId),
        //     "INVALID_REMIX_CONTRACT"
        // );

        // Verify that the authors are valid
        // _verifyDstAuthors(_authors, _dst);

        // License check
        bool _hasLicense = false;

        for (uint256 _i = 0; _i < _authors.length; _i++) {
            if (remixLicenseActive(_authors[_i])) {
                _hasLicense = true;
            }
        }

        require(_hasLicense, "MISSING_AUTHOR_LICENSE");  // TODO_ONE_DAY: Is this safe? Should require pre approval of minter?

        // Mint derivative
        _mintDerivative(_dst);
        emit DerivativeIssued(_dst);

        return true;
    }

    /// @dev Send a flag token to a child Remix in case of unfair use or other complaints
    /// @param _parents Addresses of a valid chain of parents, starting from this contract (element 0)
    ///                 all the way to the parent targetting
    function flag(address[] memory _parents) public override {
        if (msg.sender != factory) {
            _requireIsValidParentsChain(_parents);
        }

        if (!flaggingParentsExists[_parents[_parents.length -1]]) {
            flaggingParents.push(_parents[_parents.length -1]);
            flaggingParentsExists[_parents[_parents.length -1]] = true;

            emit Flagged(msg.sender, address(this));
        }
    }

    /// @dev Removes a flag token from a child Remix in case of unfair use or other complaints
    /// @param _parents Addresses of a valid chain of parents, starting from this contract (element 0)
    ///                 all the way to the parent targetting
    /// @param _index index of the parent that has flagged this Remix
    function unflag(address[] memory _parents, uint256 _index) public override {
        require(isFlagged(), "NOT_FLAGGED");

        if (msg.sender != factory) {
            _requireIsValidParentsChain(_parents);
        }

        require(
            flaggingParents[_index] == _parents[_parents.length - 1],
            "FLAGGING_PARENT_MUST_BE_THE_ONE_UNFLAGGING"
        );

        require(
            flaggingParentsExists[_parents[_parents.length -1]],
            "PARENT_HAS_NOT_FLAGGED_THE_CHILD"
        );

        flaggingParents[_index] = flaggingParents[flaggingParents.length - 1];
        flaggingParents.pop();

        // If the factory unflags a child, then the parent should not be able to flag it again
        if (msg.sender != factory) {
            flaggingParentsExists[_parents[_parents.length -1]] = false;
        }

        emit UnFlagged(msg.sender, address(this));
    }

    /// @dev Emit an event when ETH is received
    receive() external payable {
        emit RoyaltyReceived(msg.sender, msg.value);
    }


    /***************************************
        External & public view interfaces
    ****************************************/

    /// @dev Get the royalty info for a token
    /// @param _tokenId The NFT asset queried for royalty information
    /// @param _salePrice The sale price of the NFT asset specified by _tokenId
    /// @return _receiver The address which should receive the royalty payment
    /// @return _royaltyAmount The royalty payment amount for _salePrice
    function royaltyInfo(uint256 _tokenId, uint256 _salePrice)
        external
        view
        override
        returns (address _receiver, uint256 _royaltyAmount)
    {
        require(_tokenId == uint256(TokenTypes.Collectible), "NON_PURCHASABLE_TOKEN");

        return (address(this), (_salePrice * royalties) / 10000);
    }

    /// @dev Get the authors and their splits
    /// @return _authors The addresses of the authors
    /// @return _authorSplits The splits of the authors
    function getAuthorsAndSplits()
        public
        view
        returns (address[] memory _authors, uint256[] memory _authorSplits)
    {
        return (authors, authorSplits);
    }

    /// @dev Get the authors only
    /// @return _authors The addresses of the authors
    function getAuthors() public view override returns (address[] memory _authors) {
        return authors;
    }

    /// @dev Get the length of the authors array
    /// @return _authorsLength The length of the authors array
    function getAuthorsLength() public view override returns (uint256 _authorsLength) {
        return authors.length;
    }

    /// @dev Get an author from the authors array
    /// @param _index The index of the author
    /// @return _author The address of the author
    function getAuthor(uint256 _index) public view override returns (address _author) {
        return authors[_index];
    }

    /// @dev Get the parents and their splits
    /// @return _parents The addresses of the parents
    /// @return _parentSplits The splits of the parents
    function getParentsAndSplits() public view returns (address[] memory _parents, uint256[] memory _parentSplits) {
        return (parents, parentSplits);
    }

    /// @dev Get the parents only
    /// @return _parents The addresses of the parents
    function getParents() public view returns (address[] memory _parents) {
        return parents;
    }

    /// @dev Get the royalties
    /// @param _tokenAddresses The addresses of the tokens
    /// @return _addresses The addresses of the tokens linked to the royalties
    /// @return _royalties The values of the royalties
    function getRoyalties(address[] memory _tokenAddresses)
        public
        view
        returns (address[] memory _addresses, uint256[] memory _royalties)
    {
        _tokenAddresses = new address[](1);
        _royalties = new uint256[](1);
        _tokenAddresses[0] = address(0);
        _royalties[0] = address(this).balance;

        // TODO: Add royalties for ERC20 tokens

        return (_tokenAddresses, _royalties);
    }

    /// @dev Get the flagging parents
    /// @return _flaggingParents The addresses of the flagging parents
    function getFlaggingParents() public view returns (address[] memory _flaggingParents) {
        return flaggingParents;
    }

    /// @dev Check if a Remix is flagged
    /// @return _flagged if the Remix is flagged
    function isFlagged() public view returns (bool _flagged) {
        if (flaggingParents.length > 0) {
            return true;
        }

        return false;
    }

    /// @dev Check if a Remix license is active for a given address (author | countdown | RMX owner)
    /// @param _holder The address to check
    /// @return _isRemixLicenseActive if the Remix license is active
    function remixLicenseActive(address _holder) public view returns (bool _isRemixLicenseActive) {
        bool _author = false;

        for (uint i = 0; i < authors.length; i++) {
            if (_holder == authors[i]) {
                _author = true;
                break;
            }
        }

        return
            (_author) ||
            (canMintUntil[_holder] > block.timestamp) ||
            (currentRMXOwner == _holder);
    }

    /// @dev Check if a Remix usage license is active for a given address
    /// @param _holder The address to check
    /// @return _isUsageLicenseActive if the Remix usage license is active
    function usageLicenseActive(address _holder) public view returns (bool _isUsageLicenseActive) {
        if (balanceOf(_holder, uint256(TokenTypes.Usage)) > 0) {
            return true;
        }

        return false;
    }

    /// @dev Interface support
    /// @param _interfaceId The interface to check for support
    /// @return _isSupported if the interface is supported
    function supportsInterface(bytes4 _interfaceId)
        public
        view
        override (
            ERC165StorageUpgradeable,
            IERC165Upgradeable,
            ERC1155Upgradeable,
            ERC1155ReceiverUpgradeable,
            IRemix
        )
        returns (bool _isSupported)
    {
        return _interfaceId == type(IRemix).interfaceId || super.supportsInterface(_interfaceId);
    }


    /****************************
        Hooks & Internal utils
    *****************************/

    /// @dev Check if a factory is valid
    /// @param _addr The address to check
    function _verifyFactory(address _addr) internal view returns (bool _isFactory) {
        uint256 _size;
        assembly {
            _size := extcodesize(_addr)
        }

        // Ensure that it is a contract before trying to query supportsInterface
        if (_size > 0) {
            return IERC165Upgradeable(_addr).supportsInterface(type(IRemixFactory).interfaceId);
        }

        return false;
    }

    /// @dev Initialize a split sum to check that sum of all splits is 10000 (100%)
    /// @return _senderIsAuthor if the sender is an author
    function _splitSum() internal returns (bool _senderIsAuthor) {
        uint256 _sum = 0;
        _senderIsAuthor = false;

        for (uint256 _i = 0; _i < authors.length; _i++) {
            // Add splits for authors
            if (msg.sender == authors[_i]) {
                _senderIsAuthor = true;
            }

            splitAddresses.push(authors[_i]);  // Add split address for each author
            splits[authors[_i]] = authorSplits[_i];  // Add split amount for each author
            _sum += authorSplits[_i];  // Add splits to working sum
            _mintBadge(authors[_i]);  // Mint a badge to each author
        }

        for (uint256 _j = 0; _j < parents.length; _j++) {
            // Add splits for parent tokens
            splitAddresses.push(parents[_j]);  // Add split address for each parent token
            splits[parents[_j]] = parentSplits[_j];  // Add split amount for each parent token
            _sum += parentSplits[_j];  // Add splits to working sum

            // Request derivatives from each specified parent
            require(
                IRemix(parents[_j]).requestDerivative(authors, address(this))
            );
        }

        // Ensure valid split total
        require(_sum == 10000, "INVALID_SPLIT_SUM");

        return _senderIsAuthor;
    }

    /// @dev Verifies that the destination Remix contract have the same authors as the source Remix contract
    ///      in the case of a derivative request
    /// @param _authors The authors of the source Remix contract
    /// @param _dst The destination Remix contract address
    function _verifyDstAuthors(address[] memory _authors, address _dst) internal view {
        // TODO: Isaac

        // Get the authors from the destination Remix contract
        (bool _success, bytes memory _data) = address(_dst).staticcall(
            abi.encodeWithSignature("authors()")
        );

        // Ensure that the call was successful
        require(_success, "COULD_NOT_GET_DST_AUTHORS");

        // Compare the retrieved authors with _authors array
        // require(_dstAuthors.length == _authors.length, "DST_AND_SRC_AUTHORS_LEN_MISMATCH");

        // for (uint256 i = 0; i < _dstAuthors.length; i++) {
        //     require(_dstAuthors[i] == _authors[i], "DST_AND_SRC_AUTHORS_MISMATCH");
        // }
    }

    /// @dev Mint badge to license purchaser - nontransferable
    /// @param _recipient Where to send badge
    function _mintBadge(address _recipient) internal {
        _mint(_recipient, uint256(TokenTypes.Badge), 1, "");

        emit Mint(_recipient, uint256(TokenTypes.Badge));
    }

    /// @dev Mint Derivative - nontransferable
    /// @param _recipient Where to send badge
    function _mintDerivative(address _recipient) internal {
        _mint(_recipient, uint256(TokenTypes.Derivative), 1, "");

        emit Mint(_recipient, uint256(TokenTypes.Derivative));
    }

    /// @dev Mint Collectible - nontransferable
    /// @param _recipient Where to send badge
    function _mintCollectible(address _recipient) internal {
        _mint(_recipient, uint256(TokenTypes.Collectible), 1, "");

        emit Mint(_recipient, uint256(TokenTypes.Collectible));
    }

    /// @dev Mint Collectible - nontransferable
    /// @param _recipient Where to send badge
    function _mintUsage(address _recipient) internal {
        _mint(_recipient, uint256(TokenTypes.Usage), 1, "");

        emit Mint(_recipient, uint256(TokenTypes.Usage));
    }

    /// @dev Requires the sender to be an author
    /// @param _remixProxyAddress The Remix to check
    function _requireIsAuthorOf(address _remixProxyAddress) internal view {
        bool _author = false;
        address[] memory _remixAuthors = IRemix(payable(_remixProxyAddress)).getAuthors();

        for (uint256 _i; _i < _remixAuthors.length; _i++) {
            if (_remixAuthors[_i] == msg.sender) {
                _author = true;
            }
        }

        require(_author, "SENDER_IS_NOT_AN_AUTHOR");
    }

    /// @dev Requires a valid parent chain
    /// @param _parentChain The parent chain to check
    function _requireIsValidParentsChain(address[] memory _parentChain) internal view {
        _requireIsAuthorOf(_parentChain[_parentChain.length - 1]);

        for (uint256 _i = 0; _i < _parentChain.length - 1; _i++) {
            require(
                IERC1155Upgradeable(_parentChain[_i + 1]).balanceOf(_parentChain[_i], uint256(TokenTypes.Derivative)) == 1,
                "ADDRESS_IS_NOT_A_CHILD_IN_PARENTS_CHAIN"
            );
        }
    }

    /// @dev Get the factory cut from the RemixFactory
    function _getFactoryCut() internal {
        if (factory != address(0)) {
            factoryCut = IRemixFactory(factory).getFactoryCut();
        } else {
            // Fall back to 0 if no factory is set
            factoryCut = 0;
        }
    }

    /// @dev Take an ETH cut for the factory
    /// @param _amount The amount to take a cut from
    function _takeCut(uint256 _amount) internal returns (uint256 _remaining) {
        uint256 _toSend = 0;

        if (factory != address(0)) {
            _toSend = (factoryCut * _amount) / 10000;

            (bool _success, ) = factory.call{ value: _toSend }("");
            require(_success, "ETH_CUT_TRANSFER_FAILED");
        }

        return _amount - _toSend;
    }

    /// @dev Take a ERC20 cut for the factory
    /// @param _amount The amount to take a cut from
    function _takeCut(address _tokenAddress, uint256 _amount) internal returns (uint256 _remaining) {
        uint256 _toSend = 0;

        if (factory != address(0)) {
            _toSend = (factoryCut * _amount) / 10000;

            require(
                IERC20Upgradeable(_tokenAddress).transfer(factory, _toSend),
                "ERC20_CUT_TRANSFER_FAILED"
            );
        }

        return _amount - _toSend;
    }

    /// @dev Perform ETH splits
    /// @param _total The total amount to split
    function _performSplit(uint256 _total) internal {
        uint256[] memory _amounts = new uint256[](splitAddresses.length);

        for (uint256 _i = 0; _i < splitAddresses.length; _i++) {
            uint256 _toSend = (splits[splitAddresses[_i]] * _total) / 10000;
            _amounts[_i] = _toSend;

            (bool _success, ) = splitAddresses[_i].call{ value: _toSend }("");
            require(_success, "ETH_SPLIT_TRANSFER_FAILED");
        }

        emit RoyaltiesHarvested(splitAddresses, address(0), _amounts);
    }

    /// @dev Perform ERC20 splits
    /// @param _tokenAddress The address of the token to split
    /// @param _total The total amount to split
    function _performSplit(address _tokenAddress, uint256 _total) internal {
        uint256[] memory _amounts = new uint256[](splitAddresses.length);

        for (uint256 _i = 0; _i < splitAddresses.length; _i++) {
            uint256 _toSend = (splits[splitAddresses[_i]] * _total) / 10000;
            _amounts[_i] = _toSend;

            require(
                IERC20Upgradeable(_tokenAddress).transfer(splitAddresses[_i], _toSend),
                "ERC20_SPLIT_TRANSFER_FAILED"
            );
        }

        emit RoyaltiesHarvested(splitAddresses, _tokenAddress, _amounts);
    }

    /// @dev Restrict transfers to Remix & Collectible tokens
    /// @param _operator The operator
    /// @param _from The sender
    /// @param _to The recipient
    /// @param _ids The token ids
    /// @param _amounts The token amounts
    /// @param _data The data
    function _beforeTokenTransfer(
        address _operator,
        address _from,
        address _to,
        uint256[] memory _ids,
        uint256[] memory _amounts,
        bytes memory _data
    ) internal override (ERC1155SupplyUpgradeable) {
        super._beforeTokenTransfer(_operator, _from, _to, _ids, _amounts, _data);  // Trigger parent hook to track supplies

        for (uint256 _i = 0; _i < _ids.length; _i++) {
            require(
                _to == address(0) ||
                    _from == address(0) ||
                    _ids[_i] == uint256(TokenTypes.Flag) ||
                    _ids[_i] == uint256(TokenTypes.Badge) ||
                    _ids[_i] == uint256(TokenTypes.Collectible),
                "TOKEN_NOT_TRANSFERABLE"
            );

            if (_ids[_i] == uint256(TokenTypes.Collectible)) {
                currentCollectibleOwner = _to;
            }
        }
    }
}
