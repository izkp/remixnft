// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's Ownable.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol
 *
 * Using OpenZeppelin's ProxyAdmin.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/proxy/transparent/ProxyAdmin.sol
 *
 * Using OpenZeppelin's AccessControl.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/AccessControl.sol
 *
 * Using OpenZeppelin's IERC20.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol
 *
 * Using OpenZeppelin's ERC165Storage.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/utils/introspection/ERC165Storage.sol
 */

import "hardhat/console.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165Storage.sol";
import "./proxies/RemixProxy.sol";
import "./Remix.sol";


/// @title Interface for Remix Factory
interface IRemixFactory {
    /// @notice Get the Remix implementation address
    function getRemixImplementation() external view returns (address _remixImplementation);

    /// @notice Get the Factory cut
    function getFactoryCut() external view returns (uint256 _factoryCut);
}

/// @title Main Remix Factory contract
contract RemixFactory is
    IRemixFactory,
    Ownable,
    AccessControl,
    ERC165Storage,
    ProxyAdmin
{
    // Roles
    bytes32 public constant MODERATOR_ROLE = keccak256("MODERATOR_ROLE");

	// Addresses
	address private remixImplementation;
    uint256 public factoryCut;
	uint256 public count;

	// Mappings
	mapping(address => address[]) registry;

    // Role events
    event ModeratorAdded(address account);
    event ModeratorRevoked(address account);

	// Events
	event Deployed(address remixImplementation);
    event Harvested(address to, address tokenAddress, uint256 amount);
    event CutUpdated(uint256 previousFactoryCut, uint256 newFactoryCut);
    event CutReceived(address sender, uint256 amount);
	event RemixDeployed(address[] authors, address remixProxyAddress, address remixImplementation);
	event RemixRegistered(address remixProxyAddress);
    event RemixImplementationUpgraded(address prevRemixImplementation, address newRemixImplementation);

    // Modifiers
    modifier onlyStaff() {
        require(
            msg.sender == owner() ||
            hasRole(MODERATOR_ROLE, msg.sender),
            "NOT_MEMBER_OF_STAFF"
        );

        _;
    }


    /// @dev Constructor
    /// @param _factoryCut BPs shared with the factory
	constructor(uint256 _factoryCut) {
        require(_factoryCut < 10000, "INVALID_CUT");

        // Deploy Remix implementation contract
		remixImplementation = address(new Remix());

        // Emit event first for subgraph
		emit Deployed(remixImplementation);

        // Transfer the ownership
		_transferOwnership(msg.sender);

        // Set factory cut
        factoryCut = _factoryCut;

        // Set up roles
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setRoleAdmin(MODERATOR_ROLE, DEFAULT_ADMIN_ROLE);

        // Register interfaces
        _registerInterface(type(IRemixFactory).interfaceId);
	}


    /// @dev Deploys a Remix contract and registers it
    /// @param _args Struct containing Remix initializer arguments (prevent stack too deep error)
    /// @return _remixProxyAddress Address of deployed Remix proxy contract
	function deploy(
        IRemix.deployArgs memory _args
	) external returns (address _remixProxyAddress) {
		_isAuthor(_args.authors);

        // Deploy Remix proxy contract
        // Note: Initialize selector cannot be used because of "Stack too deep" error
		RemixProxy _proxy = new RemixProxy(
            address(this),
            remixImplementation,
            address(new ProxyAdmin()),
            abi.encodeWithSelector(Remix.initialize.selector, _args)
        );

        // Get Remix proxy address and register it
        _remixProxyAddress = address(_proxy);
		registerRemix(_remixProxyAddress);

		emit RemixDeployed(_args.authors, _remixProxyAddress, remixImplementation);

		return _remixProxyAddress;
	}

    /// @dev Add a Remix (proxy) to the registry
    /// @param _remixProxyAddress Address of the Remix proxy contract
	function registerRemix(address _remixProxyAddress) public {
        // Verifies that the address is a Remix contract
        require(
            IRemix(payable(_remixProxyAddress)).supportsInterface(type(IRemix).interfaceId),
            "INVALID_REMIX_CONTRACT"
        );

		address[] memory _authors;
		_authors = IRemix(payable(_remixProxyAddress)).getAuthors();

		for (uint i = 0; i < _authors.length; i++) {
			registry[_authors[i]].push(_remixProxyAddress);
		}

		count++;

		emit RemixRegistered(_remixProxyAddress);
	}

    /// @dev Upgrade Remix implementation contract and all Remix proxy contracts
    ///      using an external Remix implementation contract address
    /// @param _newRemixImplementation Address of new Remix implementation contract
    function upgradeRemixImplementation(address _newRemixImplementation) public onlyOwner {
        require(_newRemixImplementation != address(0), "NEW_IMPLEMENTATION_ADDRESS_CANNOT_BE_ZERO");
        require(_newRemixImplementation != remixImplementation, "IMPLEMENTATION_ADDRESSES_CANNOT_BE_SAME");

        // Save the previous Remix implementation address for event
        address _prevRemixImplementation = remixImplementation;

        // Change Remix implementation address
        remixImplementation = _newRemixImplementation;

        emit RemixImplementationUpgraded(_prevRemixImplementation, _newRemixImplementation);
    }

    /// @dev Harvest royalties from Remixes
    /// @param _remixes List of Remixes to harvest
    /// @param _tokenAddress Address of token to harvest
	function harvestMany(address[] memory _remixes, address _tokenAddress) public {
		for (uint256 _i = 0; _i < _remixes.length; _i++) {
			IRemix(payable(_remixes[_i])).harvestRoyalties(_tokenAddress);
		}
	}

    /// @dev Harvest from the factory
    /// @param _tokenAddress 0 address if ETH, non-0 address if ERC20
    /// @param _to Address to send ETH/ERC20 to
    function harvestFactory(address _tokenAddress, address _to) public onlyOwner {
        require(_to != address(0), "INVALID_HARVEST_ADDRESS");

        if (_tokenAddress == address(0)) {
            uint256 _balance = address(this).balance;
            require(_balance > 0, "NO_ETHER_TO_HARVEST");

            (bool _success, ) = _to.call{ value: _balance }("");
            require(_success, "ETH_TRANSFER_FAILED");

            emit Harvested(_to, address(0), _balance);
        } else {
            uint256 _balance = IERC20(_tokenAddress).balanceOf(address(this));
            require(_balance > 0, "NO_ERC20_TO_HARVEST");

            require(
                IERC20(_tokenAddress).transfer(_to, _balance),
                "ERC20_TRANSFER_FAILED"
            );

            emit Harvested(_to, _tokenAddress, _balance);
        }
    }

    /// @dev Updates the factory cut
    /// @param _newCut New cut
    function updateFactoryCut(uint256 _newCut) public onlyOwner {
        require(_newCut < 10000, "INVALID_CUT");

        uint256 _previousCut = factoryCut;
        factoryCut = _newCut;

        emit CutUpdated(_previousCut, _newCut);
    }

    /// @dev Flag a Remix
    /// @param _remixProxyAddress Address of Remix proxy to flag
	function flag(address _remixProxyAddress) public onlyStaff {
		address[] memory _args = new address[](2);
		_args[0] = _remixProxyAddress;
		_args[1] = address(this);

		IRemix(payable(_remixProxyAddress)).flag(_args);
	}

    /// @dev Unflag a Remix
    /// @param _remixProxyAddress Address of Remix proxy to unflag
    /// @param _index Index of Remix in RemixFactory's flagged list
	function unflag(address _remixProxyAddress, uint256 _index) public onlyStaff {
		address[] memory _args = new address[](2);
		_args[0] = _remixProxyAddress;
		_args[1] = address(this);

		IRemix(payable(_remixProxyAddress)).unflag(_args, _index);
	}

    /// @dev Emit an event when ETH is received
    receive() external payable {
        emit CutReceived(msg.sender, msg.value);
    }


    /***************************************
        External & public view interfaces
    ****************************************/

    /// @dev Get the Remix implementation address
    /// @return _remixImplementation Remix implementation address
    function getRemixImplementation() external override view returns (address _remixImplementation) {
        return remixImplementation;
    }

    /// @dev Get Remixes by author
    /// @param _author Address of the author
    /// @return _remixProxyAddresses List of Remix proxies created by the author
	function getRemixByAuthor(address _author) public view returns (address[] memory _remixProxyAddresses) {
		return registry[_author];
	}

    /// @dev Get the Factory cut
    /// @return _factoryCut Factory cut
    function getFactoryCut() external override view returns (uint256 _factoryCut) {
        return factoryCut;
    }

    /// @dev Interface support
    /// @param _interfaceId The interface to check for support
    /// @return _isSupported if the interface is supported
    function supportsInterface(bytes4 _interfaceId)
        public
        view
        override (
            ERC165Storage,
            AccessControl
        )
        returns (bool _isSupported)
    {
        return _interfaceId == type(IRemixFactory).interfaceId || super.supportsInterface(_interfaceId);
    }


    /****************************
        Hooks & Internal utils
    *****************************/

    /// @dev Check if a list of addresses contains the sender
    /// @param _authors List of addresses to check
	function _isAuthor(address[] memory _authors) internal view {
		bool _isSenderAuthor = false;

		for (uint256 _i = 0; _i < _authors.length; _i++) {
			if (_authors[_i] == msg.sender) {
				_isSenderAuthor = true;
			}
		}

		require(_isSenderAuthor, "SENDER_IS_NOT_AUTHOR");
	}


    /*****************************
        Moderator & Staff utils
    ******************************/

    /// @dev Add or revoke moderators
    /// @param _account Address of the moderator
    function addOrRevokeModerator(address _account) public onlyOwner {
        if (!hasRole(MODERATOR_ROLE, _account)) {
            grantRole(MODERATOR_ROLE, _account);

            emit ModeratorAdded(_account);
        } else {
            revokeRole(MODERATOR_ROLE, _account);

            emit ModeratorRevoked(_account);
        }
    }

    /// @dev Check if an address has moderator role
    /// @param _account Address to check
    /// @return _hasModeratorRole True if address has moderator role
    function hasModeratorRole(address _account) public view returns (bool _hasModeratorRole) {
        return hasRole(MODERATOR_ROLE, _account);
    }
}
