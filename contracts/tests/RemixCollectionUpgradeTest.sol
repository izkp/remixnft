// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


import "../RemixCollection.sol";


contract RemixCollectionUpgradeTest is RemixCollection {
    function testUpgrade() public pure returns (string memory) {
        return "testUpgrade";
    }
}