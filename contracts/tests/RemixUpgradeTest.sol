// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


import "../Remix.sol";


contract RemixUpgradeTest is Remix {
    function testUpgrade() public pure returns (string memory) {
        return "testUpgrade";
    }
}