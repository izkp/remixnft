// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's TransparentUpgradeableProxy.sol
 * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/proxy/transparent/TransparentUpgradeableProxy.sol
 */

import "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol";
import { IRemixFactory } from "../RemixFactory.sol";


/// @title Main RemixProxy contract
contract RemixProxy is TransparentUpgradeableProxy {
    // Addresses
    address public factory;


    constructor(
        address _factory,
        address _logic,
        address _admin,
        bytes memory _data
    ) payable TransparentUpgradeableProxy(_logic, _admin, _data) {
        factory = _factory;
    }


    /// @dev Returns the current implementation.
    /// @return _remixImplementation The address of the current implementation
    function _implementation() internal view override returns (address _remixImplementation) {
        // Get the current implementation from the RemixFactory
        _remixImplementation = IRemixFactory(payable(factory)).getRemixImplementation();

        return _remixImplementation;
    }
}