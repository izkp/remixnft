import fs from "fs";

import { JsonRpcSigner, TransactionReceipt } from "@ethersproject/providers";
import { HardhatEthersHelpers } from "@nomiclabs/hardhat-ethers/types";
import * as bip39 from "bip39";
import dotenv from "dotenv";
import * as EthUtil from "ethereumjs-util";
import hdkey from "ethereumjs-wallet/dist/hdkey";
import { utils } from "ethers";
import { RLP } from "ethers/lib/utils";
import { HardhatUserConfig, task } from "hardhat/config";
import "hardhat-deploy";
import "@nomicfoundation/hardhat-chai-matchers";
import "@nomiclabs/hardhat-ethers";
import "@nomiclabs/hardhat-etherscan";
import keccak from "keccak";
import * as qrcode from "qrcode-terminal";

import "@openzeppelin/hardhat-upgrades";
import "@nomicfoundation/hardhat-toolbox";
import logger from "utils/logger";

import networks from "./networks.config";
import NsContracts from "./types/contracts";

import "hardhat-contract-sizer";

// Loading the .env
dotenv.config({ path: __dirname + "/.env" });

// Mnemonic paths
const accountsPath = __dirname + "/accounts";
const mnemonicPath = __dirname + "/accounts/mnemonic.txt";

// Get the debugging state
const DEBUG = process.env.DEBUG_HARDHAT_CONFIG === "true";
const ETHERSCAN_API = process.env.ETHERSCAN_API ?? "";
const POLYGONSCAN_API = process.env.ETHERSCAN_API ?? "";

// Selects the default network
let defaultNetwork = "localhost";
if (process.env.HARDHAT_NETWORK) {
    defaultNetwork = process.env.HARDHAT_NETWORK;
}

logger.info(`Default network set to '${defaultNetwork}'`);

// Mnemonic logging
if (!fs.existsSync(mnemonicPath)) {
    logger.warn("No mnemonic.txt file found");
} else {
    const mnemonic = fs.readFileSync(mnemonicPath).toString().trim();

    if (mnemonic.split(" ").length !== 12) {
        logger.warn("Mnemonic should be 12 words");
    } else {
        logger.info(`Mnemonic: ${mnemonic}`);
    }
}

/**
 * Function used to debug the config0.
 * To use it, change the DEBUG_HARDHAT_CONFIG environment variable to true.
 * @param text The text to be printed.
 */
function debug(text: string): void {
    if (DEBUG) {
        logger.debug(text);
    }
}

/**
 * Send a transaction to the network.
 * @param signer The signer to use to send the transaction.
 * @param txParams The transaction parameters.
 * @returns The transaction receipt or never.
 */
async function send(signer: JsonRpcSigner, txParams: NsContracts.IsTx): Promise<TransactionReceipt | undefined> {
    const tx = await signer.sendTransaction(txParams);
    const receipt = await tx.wait();

    if (receipt.status === 0) {
        debug(`Transaction failed: ${tx.hash}`);
    } else {
        debug(`Successful transaction: ${tx.hash}`);
        return receipt;
    }

    return;
}

/**
 * Get the address from a string (normalizes addresses).
 * @param ethers Hardhat ethers helpers.
 * @param addr The address to normalize.
 * @returns The normalized address.
 */
async function addr(ethers: HardhatEthersHelpers, addr: string): Promise<string> {
    if (utils.isAddress(addr)) {
        return utils.getAddress(addr);
    }

    const accounts = await ethers.provider.listAccounts();

    if (addr in accounts) {
        return addr;
    }

    throw `Could not normalize address: ${addr}`;
}

task("wallet", "Create a wallet (pk) link", async (_, { ethers }) => {
    const randomWallet = ethers.Wallet.createRandom();
    const privateKey = randomWallet._signingKey().privateKey;

    logger.info(
        `Wallet generated as ${randomWallet.address} with the address:\n` +
        `http://localhost:3000/pk#${privateKey}`
    );
});

task("fundedWallet", "Create a wallet (pk) link and fund it with deployer?")
    .addOptionalParam(
        "amount",
        "Amount of ETH to send to wallet after generating"
    )
    .addOptionalParam("url", "URL to add pk to")
    .setAction(async (taskArgs, { ethers }) => {
        const randomWallet = ethers.Wallet.createRandom();
        const privateKey = randomWallet._signingKey().privateKey;

        logger.info(`Wallet generated as ${randomWallet.address}`);

        const url = taskArgs.url ? taskArgs.url : "http://localhost:3000";

        let localDeployerMnemonic;
        try {
            localDeployerMnemonic = fs.readFileSync(mnemonicPath);
            localDeployerMnemonic = localDeployerMnemonic.toString().trim();
        } catch (e) {
            /* do nothing - this file isn't always there */
            logger.info("No mnemonic.txt file found");
        }

        const amount = taskArgs.amount ? taskArgs.amount : "0.01";
        const tx: NsContracts.IsTx = {
            to: randomWallet.address,
            value: ethers.utils.parseEther(amount).toString(),
        };

        if (localDeployerMnemonic) {
            logger.info(
                `Sending ${amount} ETH to ${randomWallet.address} using deployer account:\n` +
                `${url}/pk#${privateKey}\n`
            );

            return;
        } else {
            logger.info(
                `Sending ${amount} ETH to ${randomWallet.address} using local node:\n` +
                `${url}/pk#${privateKey}\n`
            );

            return send(ethers.provider.getSigner(), tx);
        }
    });

task(
    "generate",
    "Create a mnemonic for builder deploys",
    async () => {
        const mnemonic = bip39.generateMnemonic();
        debug(`Mnemonic: ${mnemonic}`);

        const seed = await bip39.mnemonicToSeed(mnemonic);
        debug(`Seed: ${seed}`);

        const hdwallet = hdkey.fromMasterSeed(seed);
        const wallet_hdpath = "m/44'/60'/0'/0/";
        const account_index = 0;

        const fullPath = wallet_hdpath + account_index;
        debug(`Full path: ${fullPath}`);

        const wallet = hdwallet.derivePath(fullPath).getWallet();

        const privateKey = "0x" + wallet.getPrivateKeyString();
        debug(`Private Key: ${privateKey}`);

        const address = "0x" + EthUtil.privateToAddress(wallet.getPrivateKey()).toString("hex");

        logger.info(
            `Account generated as ${address} and set as mnemonic in packages/hardhat\n` +
            "Use 'yarn run account' to get more info about the deployment account."
        );

        if (!fs.existsSync(accountsPath)) {
            fs.mkdirSync(accountsPath);
        }

        fs.writeFileSync(mnemonicPath, mnemonic.toString());
        fs.writeFileSync(accountsPath + "/" + address + ".txt", mnemonic.toString());
    }
);

task(
    "mineContractAddress",
    "Looks for a deployer account that will give leading zeros"
)
    .addParam("searchFor", "String to search for")
    .setAction(async (taskArgs, { }) => {
        let contract_address = "";
        let mnemonic = "";
        let address;

        while (contract_address.indexOf(taskArgs.searchFor) != 0) {
            mnemonic = bip39.generateMnemonic();
            debug(`Mnemonic: ${mnemonic}`);

            const seed = await bip39.mnemonicToSeed(mnemonic);
            debug(`Seed: ${seed}`);

            const hdwallet = hdkey.fromMasterSeed(seed);
            const wallet_hdpath = "m/44'/60'/0'/0/";
            const account_index = 0;

            const fullPath = wallet_hdpath + account_index;
            debug(`Full path: ${fullPath}`);

            const wallet = hdwallet.derivePath(fullPath).getWallet();
            const privateKey = "0x" + wallet.getPrivateKeyString();
            debug(`Private Key: ${privateKey}`);

            address = "0x" + EthUtil.privateToAddress(wallet.getPrivateKey()).toString("hex");

            const nonce = 0x00; //The nonce must be a hex literal!
            const sender = address;

            const input_arr = [sender, nonce];
            const rlp_encoded = RLP.encode(input_arr);

            const contract_address_long = keccak("keccak256")
                .update(rlp_encoded)
                .digest("hex");

            contract_address = contract_address_long.substring(24); //Trim the first 24 characters.
        }

        logger.info(
            `Account mined as ${address} and set as mnemonic in packages/hardhat\n` +
            `This will create the first contract: 0x${contract_address}\n` +
            "Use 'yarn run account' to get more info about the deployment account."
        );

        fs.writeFileSync(
            "./" + address + "_produces" + contract_address + ".txt",
            mnemonic.toString()
        );

        fs.writeFileSync(mnemonicPath, mnemonic.toString());
    });

task(
    "account",
    "Get balance information for the deployment account.",
    async (_, { ethers }) => {
        const mnemonic = fs.readFileSync(mnemonicPath).toString().trim();
        debug(`Mnemonic: ${mnemonic}`);

        const seed = await bip39.mnemonicToSeed(mnemonic);
        debug(`Seed: ${seed}`);

        const hdwallet = hdkey.fromMasterSeed(seed);
        const wallet_hdpath = "m/44'/60'/0'/0/";
        const account_index = 0;

        const fullPath = wallet_hdpath + account_index;
        debug(`Full path: ${fullPath}`);

        const wallet = hdwallet.derivePath(fullPath).getWallet();
        const privateKey = "0x" + wallet.getPrivateKeyString();
        debug(`Private Key: ${privateKey}`);

        const address = "0x" + EthUtil.privateToAddress(wallet.getPrivateKey()).toString("hex");

        qrcode.generate(address);
        logger.info(`Deployer Account: ${address}`);

        for (const n in networks) {
            const network = networks[n];

            try {
                const provider = new ethers.providers.JsonRpcProvider(network.url);
                const balance = await provider.getBalance(address);

                logger.info(
                    `======== ${n} ========\n` +
                    `> balance: ${ethers.utils.formatEther(balance)}\n` +
                    `> nonce: ${await provider.getTransactionCount(address)}\n`
                );
            } catch (e) {
                if (DEBUG) {
                    logger.debug(e);
                }
            }
        }
    }
);

task("accounts", "Prints the list of accounts", async (_, { ethers }) => {
    const accounts = await ethers.provider.listAccounts();
    accounts.forEach((account) => logger.info(account));
});

task("blockNumber", "Prints the block number", async (_, { ethers }) => {
    const blockNumber = await ethers.provider.getBlockNumber();
    logger.info(blockNumber);
});

task("balance", "Prints an account's balance")
    .addPositionalParam("account", "The account's address")
    .setAction(async (taskArgs, { ethers }) => {
        const balance = await ethers.provider.getBalance(
            await addr(ethers, taskArgs.account)
        );
        logger.info(`${utils.formatUnits(balance, "ether")} ETH`);
    });

task("send", "Send ETH")
    .addParam("from", "From address or account index")
    .addOptionalParam("to", "To address or account index")
    .addOptionalParam("amount", "Amount to send in ether")
    .addOptionalParam("data", "Data included in transaction")
    .addOptionalParam("gasPrice", "Price you are willing to pay in gwei")
    .addOptionalParam("gasLimit", "Limit of how much gas to spend")

    .setAction(async (taskArgs, { network, ethers }) => {
        const from = await addr(ethers, taskArgs.from);
        debug(`Normalized from address: ${from}`);
        const fromSigner: JsonRpcSigner = ethers.provider.getSigner(from);

        let to;
        if (taskArgs.to) {
            to = await addr(ethers, taskArgs.to);
            debug(`Normalized to address: ${to}`);
        }

        const txRequest = {
            from: await fromSigner.getAddress(),
            to,
            value: utils.parseUnits(
                taskArgs.amount ? taskArgs.amount : "0",
                "ether"
            ).toHexString(),
            nonce: await fromSigner.getTransactionCount(),
            gasPrice: utils.parseUnits(
                taskArgs.gasPrice ? taskArgs.gasPrice : "1.001",
                "gwei"
            ),
            gasLimit: taskArgs.gasLimit ? taskArgs.gasLimit : 24000,
            chainId: network.config.chainId,
            data: undefined
        };

        if (taskArgs.data !== undefined) {
            txRequest.data = taskArgs.data;
            debug(`Adding data to payload: ${txRequest.data}`);
        }

        debug(txRequest.gasPrice.div(1000000000) + " gwei");
        debug(JSON.stringify(txRequest, null, 2));

        return await send(fromSigner, txRequest);
    });

const config: HardhatUserConfig = {
    solidity: {
        compilers: [
            {
                version: "0.8.4",
                settings: {
                    optimizer: {
                        enabled: true,
                        runs: 200,
                    },
                },
            },
            {
                version: "0.6.7",
                settings: {
                    optimizer: {
                        enabled: true,
                        runs: 200,
                    },
                },
            },
        ],
    },
    namedAccounts: {
        deployer: {
            default: 0,
        },
    },
    paths: {
        root: "./",
        tests: "./tests",
    },
    defaultNetwork: defaultNetwork,
    networks: networks,
    etherscan: {
        apiKey: {
            mainnet: ETHERSCAN_API,
            goerli: ETHERSCAN_API,
            polygon: POLYGONSCAN_API,
            polygonMumbai: POLYGONSCAN_API
        }
    },
};


export default config;