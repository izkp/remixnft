import fs from "fs";

import NsContracts from "./types/contracts";


/**
 * Returns the mnemonic from the environment variable MNEMONIC.
 */
function mnemonic(): string {
    const mnemonicPath = __dirname + "/accounts/mnemonic.txt";

    if (fs.existsSync(mnemonicPath)) {
        const mnemonic = fs.readFileSync(mnemonicPath).toString().trim();

        return mnemonic;
    } else {
        return "";
    }
}

/**
 * List of available networks:
 * - localhost: Localhost 8545
 * - ganache: Localhost 8545  (with chainId 1337)
 * - mainnet: Ethereum mainnet
 * - goerli: Goerli testnet
 * - polygon: Polygon mainnet
 * - polygonMumbai: Polygon Mumbai testnet
 */
const networks: NsContracts.IsNetworks = {
    localhost: {
        url: "http://localhost:8545",
        chainId: 31337
    },
    ganache: {
        url: "http://localhost:8545",
        chainId: 1337
    },
    mainnet: {
        url: "https://mainnet.infura.io/v3/"+process.env.INFURA_KEY,
        accounts: {
            mnemonic: mnemonic(),
        }
    },
    goerli: {
        url: "https://goerli.infura.io/v3/"+process.env.INFURA_KEY,
        accounts: {
            mnemonic: mnemonic(),
        }
    },
    polygon: {
        url: "https://rpc-mainnet.maticvigil.com/",
        gasPrice: 1000000000,
        accounts: {
            mnemonic: mnemonic(),
        }
    },
    polygonMumbai: {
        url: "https://rpc-mumbai.maticvigil.com/",
        gasPrice: 1000000000,
        accounts: {
            mnemonic: mnemonic(),
        }
    }
};


export default networks;