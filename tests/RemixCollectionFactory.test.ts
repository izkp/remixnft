import { Provider } from "@ethersproject/abstract-provider/src.ts/index";
import { Signer } from "@ethersproject/abstract-signer/src.ts/index";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
// import { BigNumber, utils } from "ethers";
import { ethers } from "hardhat";

import { ADDRESS_ZERO, remixCollectionBuildArgs } from "helpers/tests";
// import NsRemixCollection from "types/RemixCollection";
import NsRemixCollectionFactory from "types/RemixCollectionFactory";
import NsRemixCollectionUpgradeTest from "types/tests/RemixCollectionUpgradeTest";

import { RemixCollection as IsRemixCollection } from "../typechain-types/contracts/RemixCollection";
import { RemixCollectionFactory as IsRemixCollectionFactory } from "../typechain-types/contracts/RemixCollectionFactory.sol/RemixCollectionFactory";
import { RemixCollectionUpgradeTest as IsRemixCollectionUpgradeTest } from "../typechain-types/contracts/tests/RemixCollectionUpgradeTest";


describe("RemixCollectionFactory", () => {
    // Remix Collection Factory
    let RemixCollectionFactory: IsRemixCollectionFactory;
    let RemixCollectionFactory1: NsRemixCollectionFactory.IsWrapper;

    // Remix Collection upgrade test
    let RemixCollectionUpgradeTest: IsRemixCollectionUpgradeTest;
    let RemixCollectionUpgradeTest1: NsRemixCollectionUpgradeTest.IsWrapper;

    let args: NsRemixCollectionFactory.IsContractBuildArgs;
    let events;

    // Signers
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addr2: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addrs: SignerWithAddress[];

    /**
     * Deploys a RemixCollectionFactory contract.
     * @param signer The signer to use.
     * @returns The RemixCollectionFactory contract.
     */
    const deployRemixCollectionFactory = async (
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixCollectionFactory.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixCollectionFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollectionFactory.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollectionFactory.deploy();
        }
    };

    /**
     * Deploys a RemixCollection contract from a RemixCollectionFactory contract.
     * With the owner address as the contract owner.
     * @param factory The RemixCollectionFactory contract.
     * @returns The RemixCollection proxy contract address.
     */
    const deployRemixCollectionFromFactory = async (
        factory: NsRemixCollectionFactory.IsWrapper,
        signer: Signer | Provider | string | undefined = owner
    ): Promise<string> => {
        args = remixCollectionBuildArgs();

        const tx = await factory.connect(signer).deploy(args);
        const receipt = await tx.wait();

        const event = receipt.events?.find((e) => e.event === "RemixCollectionDeployed");
        const proxyAddress = event?.args?.remixCollectionProxyAddress;

        return proxyAddress as string;
    };

    /**
     * Deploys a RemixCollectionUpgradeTest contract with no args.
     * @param signer The signer to use.
     * @returns The RemixCollection implementation contract.
     */
    const deployRemixCollectionUpgradeTestWithNoArgs = async (
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixCollectionUpgradeTest.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixCollectionFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollectionUpgradeTest.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollectionUpgradeTest.deploy();
        }
    };

    /**
     * Deploy a RemixCollection implementation contract with args.
     * @param deployArgs The args to use.
     * @param signer The signer to use.
     * @returns The RemixCollection implementation contract.
     */
    const deployRemixCollectionUpgradeTest = async (
        deployArgs: NsRemixCollectionUpgradeTest.IsContractBuildArgs,
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixCollectionUpgradeTest.IsWrapper> => {
        let contract: NsRemixCollectionUpgradeTest.IsWrapper;

        if (signer) {
            contract = await deployRemixCollectionUpgradeTestWithNoArgs(signer);
            await contract.initialize(deployArgs);
        } else {
            contract = await deployRemixCollectionUpgradeTestWithNoArgs();
            await contract.initialize(deployArgs);
        }

        return contract;
    };

    // TODO: Deploy an ERC20 contract

    beforeEach(async () => {
        // Get the ContractFactory and Signers here
        RemixCollectionFactory = await ethers.getContractFactory("RemixCollectionFactory") as unknown as IsRemixCollectionFactory;
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    });

    it("Should deploy a RemixCollectionFactory", async () => {
        RemixCollectionFactory1 = await deployRemixCollectionFactory();
    });

    it("Should deploy a RemixCollectionFactory and emit a Deployed event", async () => {
        RemixCollectionFactory1 = await deployRemixCollectionFactory();

        events = await RemixCollectionFactory1.queryFilter("Deployed");
        expect(events, "Deployed event missing")
            .to.have.length(1);
    });

    it("Should deploy a RemixCollectionFactory and emit an unique Deployed event", async () => {
        RemixCollectionFactory1 = await deployRemixCollectionFactory();

        events = await RemixCollectionFactory1.queryFilter("Deployed");
        expect(events.length, "Deployed event number is not correct")
            .to.eql(1);
    });

    it("Should set the owner of the RemixCollectionFactory", async () => {
        RemixCollectionFactory1 = await deployRemixCollectionFactory();

        expect(await RemixCollectionFactory1.owner(), "Owner is not correct")
            .to.eql(owner.address);
    });

    it("Should set the owner of the RemixCollectionFactory as admin (AccessControl)", async () => {
        RemixCollectionFactory1 = await deployRemixCollectionFactory();
        const adminRoleKeccak = await RemixCollectionFactory1.DEFAULT_ADMIN_ROLE();

        expect(await RemixCollectionFactory1.hasRole(adminRoleKeccak, owner.address), "Owner is not admin")
            .to.eql(true);
    });

    describe("deploy()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            RemixCollectionFactory = await ethers.getContractFactory("RemixCollectionFactory") as unknown as IsRemixCollectionFactory;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

            RemixCollectionFactory1 = await deployRemixCollectionFactory();
        });

        it("Should return the proxy address", async () => {
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            expect(proxyAddress, "Proxy address is not correct")
                .to.not.be.undefined;
        });

        it("Should register the RemixCollection and emit a RemixCollectionRegistered event", async () => {
            args = remixCollectionBuildArgs();
            await RemixCollectionFactory1.deploy(args);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionRegistered");
            expect(events, "RemixCollectionRegistered event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixCollectionDeployed event", async () => {
            args = remixCollectionBuildArgs();
            await RemixCollectionFactory1.deploy(args);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionDeployed");
            expect(events, "RemixCollectionDeployed event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixCollectionDeployed event that contains the sender, the proxy and the implementation addresses", async () => {
            args = remixCollectionBuildArgs();
            await RemixCollectionFactory1.deploy(args);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionDeployed");
            expect(events[0].args.sender, "Sender address is not correct")
                .to.eql(owner.address);
            expect(events[0].args.remixCollectionImplementation, "Implementation address is not correct")
                .to.not.be.undefined;
        });

        it("Should return the correct proxy address (checked by calling a public RemixCollection function)", async () => {
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            // Getting the complete RemixCollection contract
            const RemixCollectionProxyObj = await ethers.getContractFactory("RemixCollection") as unknown as IsRemixCollection;
            const RemixCollectionProxy = RemixCollectionProxyObj.attach(proxyAddress) as unknown as IsRemixCollection;

            // Calling getStatus() to check contract interaction with the proxy
            expect(await RemixCollectionProxy.getStatus(), "Status is not correct").
                to.be.equal(0);  // 0 -> Open
        });

        it("Should have the correct factory address (RemixCollection -> factory public var)", async () => {
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            // Getting the complete RemixCollection contract
            const RemixCollectionProxyObj = await ethers.getContractFactory("RemixCollection") as unknown as IsRemixCollection;
            const RemixCollectionProxy = RemixCollectionProxyObj.attach(proxyAddress) as unknown as IsRemixCollection;

            const factoryAddressFromProxy = await RemixCollectionProxy.factory();
            expect(factoryAddressFromProxy, "Factory address is not correct")
                .to.eql(RemixCollectionFactory1.address);
        });
    });

    describe("registerCollection()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            RemixCollectionFactory = await ethers.getContractFactory("RemixCollectionFactory") as unknown as IsRemixCollectionFactory;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

            RemixCollectionFactory1 = await deployRemixCollectionFactory();
        });

        it("Should require the address to be a RemixCollection contract", async () => {
            await expect(RemixCollectionFactory1.registerRemixCollection(addr1.address))
                .to.be.rejected;
        });

        it("Should add the RemixCollection to the list of registered Remix Collections (registry)", async () => {
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            expect(await RemixCollectionFactory1.getRemixCollectionByAuthor(owner.address), "RemixCollection not registered")
                .to.eql([proxyAddress]);
        });

        it("Should emit a RemixCollectionRegistered event", async () => {
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            await expect(RemixCollectionFactory1.registerRemixCollection(proxyAddress))
                .to.emit(RemixCollectionFactory1, "RemixCollectionRegistered");
        });
    });

    describe("upgradeCollectionImplementation()", () => {
        beforeEach(async () => {
            RemixCollectionFactory1 = await deployRemixCollectionFactory();

            // Get the ContractFactory here.
            RemixCollectionUpgradeTest = await ethers.getContractFactory("RemixCollectionUpgradeTest") as unknown as IsRemixCollectionUpgradeTest;
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixCollectionFactory1.connect(addr1).upgradeRemixCollectionImplementation(ADDRESS_ZERO))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("(INTERNAL VERIFICATION) Should deploy a new RemixCollection implementation contract based on RemixCollectionUpgradeTest", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            expect(RemixCollectionUpgradeTest1.address, "Implementation address is not correct")
                .to.not.be.undefined;
        });

        it("(INTERNAL VERIFICATION) Should deploy an extension of the RemixCollection contract", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            expect(await RemixCollectionUpgradeTest1.getStatus(), "Status is not correct").
                to.be.equal(0);  // 0 -> Open
        });

        it("Should revert if the implementation address is address(0)", async () => {
            await expect(RemixCollectionFactory1.upgradeRemixCollectionImplementation(ADDRESS_ZERO))
                .to.be.rejectedWith("NEW_IMPLEMENTATION_ADDRESS_CANNOT_BE_ZERO");
        });

        it("Should revert if the implementation address is the same as the current one", async () => {
            const currentImplementationAddress = await RemixCollectionFactory1.getRemixCollectionImplementation();

            await expect(RemixCollectionFactory1.upgradeRemixCollectionImplementation(currentImplementationAddress))
                .to.be.rejectedWith("IMPLEMENTATION_ADDRESSES_CANNOT_BE_SAME");
        });

        it("Should change the RemixCollection implementation address", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            const newImplementationAddress = await RemixCollectionFactory1.getRemixCollectionImplementation();
            expect(newImplementationAddress, "Implementation address is not correct")
                .to.eql(RemixCollectionUpgradeTest1.address);
        });

        it("Should emit a RemixCollectionImplementationUpgraded event", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionImplementationUpgraded");
            expect(events, "RemixCollectionImplementationUpgraded event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixCollectionImplementationUpgraded event that contains the previous implementation address", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionImplementationUpgraded");
            expect(events[0].args.prevRemixCollectionImplementation, "Previous implementation address is not correct")
                .to.not.be.undefined;
        });

        it("Should emit a RemixCollectionImplementationUpgraded event that contains the new implementation address", async () => {
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionImplementationUpgraded");
            expect(events[0].args.newRemixCollectionImplementation, "New implementation address is not correct")
                .to.eql(RemixCollectionUpgradeTest1.address);
        });

        it("Should allow interactions with a proxy deployed after an implementation address update", async () => {
            // Deploy the upgrade test contract for the new RemixCollection implementation
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            // Upgrade the RemixCollection implementation
            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            // Deploy a new proxy
            args = remixCollectionBuildArgs();
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            // Getting the complete RemixCollection contract
            const RemixCollectionProxyObj = await ethers.getContractFactory("RemixCollection") as unknown as IsRemixCollection;
            const RemixCollectionProxy = RemixCollectionProxyObj.attach(proxyAddress) as unknown as IsRemixCollection;

            // Calling getStatus() to check contract interaction with the proxy
            expect(await RemixCollectionProxy.getStatus(), "Status is not correct").
                to.be.equal(0);  // 0 -> Open
        });

        it("Should upgrade the previously deployed proxies and allow to call new functions on them", async () => {
            // Deploy a RemixCollection proxy
            args = remixCollectionBuildArgs();
            const proxyAddress1 = await deployRemixCollectionFromFactory(RemixCollectionFactory1);

            // Deploy the upgrade test contract for the new RemixCollection implementation
            args = remixCollectionBuildArgs();
            RemixCollectionUpgradeTest1 = await deployRemixCollectionUpgradeTest(args);

            // Upgrade the RemixCollection implementation
            await RemixCollectionFactory1.upgradeRemixCollectionImplementation(RemixCollectionUpgradeTest1.address);

            // Getting the complete RemixCollection contract (with the new implementation)
            const newRemixCollectionProxy = RemixCollectionUpgradeTest.attach(proxyAddress1) as unknown as IsRemixCollectionUpgradeTest;

            // Calling testUpgrade() to check contract interaction with the proxy
            expect(await newRemixCollectionProxy.testUpgrade(), "Test upgrade is not correct").
                to.be.eql("testUpgrade");
        });
    });

    describe("getCollectionImplementation()", () => {
        beforeEach(async () => {
            RemixCollectionFactory1 = await deployRemixCollectionFactory();
        });

        it("Should return the same RemixCollection implementation address as the RemixCollectionDeployed event", async () => {
            args = remixCollectionBuildArgs();
            await RemixCollectionFactory1.deploy(args);

            events = await RemixCollectionFactory1.queryFilter("RemixCollectionDeployed");
            expect(await RemixCollectionFactory1.getRemixCollectionImplementation(), "Implementation address is not correct")
                .to.eql(events[0].args.remixCollectionImplementation);
        });
    });

    describe("getRemixCollectionByAuthor()", () => {
        beforeEach(async () => {
            RemixCollectionFactory1 = await deployRemixCollectionFactory(addr1);
        });

        it("Should return an empty array if the author never deployed any collection on the factory", async () => {
            expect(await RemixCollectionFactory1.getRemixCollectionByAuthor(addr2.address), "Array is not empty")
                .to.have.length(0);
        });

        it("Should return an array with the deployed collections by the author", async () => {
            args = remixCollectionBuildArgs();
            await RemixCollectionFactory1.deploy(args);

            expect(await RemixCollectionFactory1.getRemixCollectionByAuthor(addr1.address), "Array is not correct")
                .to.have.length(1);
        });

        it("Should return the correct array of deployed collections by the author", async () => {
            const proxyAddress = await deployRemixCollectionFromFactory(RemixCollectionFactory1, addr1);

            expect(await RemixCollectionFactory1.getRemixCollectionByAuthor(addr1.address), "Array is not correct")
                .to.be.eql([proxyAddress]);
        });
    });

    describe("addOrRevokeModerator()", () => {
        beforeEach(async () => {
            RemixCollectionFactory1 = await deployRemixCollectionFactory();
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixCollectionFactory1.connect(addr1).addOrRevokeModerator(addr1.address))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should add a moderator and emit a ModeratorAdded event", async () => {
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixCollectionFactory1.queryFilter("ModeratorAdded");
            expect(events, "ModeratorAdded event missing")
                .to.have.length(1);
        });

        it("Should add a moderator and emit a ModeratorAdded event that contains the added moderator address", async () => {
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixCollectionFactory1.queryFilter("ModeratorAdded");
            expect(events[0].args.account, "Moderator address is not correct")
                .to.eql(addr1.address);
        });

        it("Should revoke a moderator and emit a ModeratorRevoked event", async () => {
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixCollectionFactory1.queryFilter("ModeratorRevoked");
            expect(events, "ModeratorRevoked event missing")
                .to.have.length(1);
        });

        it("Should revoke a moderator and emit a ModeratorRevoked event that contains the revoked moderator address", async () => {
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixCollectionFactory1.queryFilter("ModeratorRevoked");
            expect(events[0].args.account, "Moderator address is not correct")
                .to.eql(addr1.address);
        });
    });

    describe("hasModeratorRole()", () => {
        beforeEach(async () => {
            RemixCollectionFactory1 = await deployRemixCollectionFactory();
        });

        it("Should return true if the address has the moderator role", async () => {
            await RemixCollectionFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            expect(await RemixCollectionFactory1.hasModeratorRole(addr1.address), "Not moderator but should be")
                .to.eql(true);
        });

        it("Should return false if the address does not have the moderator role", async () => {
            expect(await RemixCollectionFactory1.hasModeratorRole(addr1.address), "Moderator but should not be")
                .to.eql(false);
        });
    });
});