import { Provider } from "@ethersproject/abstract-provider/src.ts/index";
import { Signer } from "@ethersproject/abstract-signer/src.ts/index";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { BigNumber, utils } from "ethers";
import { ethers } from "hardhat";

import { ADDRESS_ZERO, remixBuildArgs, remixCollectionBuildArgs } from "helpers/tests";
import NsRemix from "types/Remix";
import NsRemixCollection from "types/RemixCollection";

import { Remix as IsRemix } from "../typechain-types/contracts/Remix.sol/Remix";
import { RemixCollection as IsRemixCollection } from "../typechain-types/contracts/RemixCollection";


describe("RemixCollection", () => {
    // Remix
    let Remix: IsRemix;
    let Remix1: NsRemix.IsWrapper;
    let Remix2: NsRemix.IsWrapper;
    let Remix3: NsRemix.IsWrapper;

    // Remix Collection
    let RemixCollection: IsRemixCollection;
    let RemixCollection1: NsRemixCollection.IsWrapper;

    let overrides: NsRemixCollection.IsPayableOverrides;
    let args: NsRemixCollection.IsContractBuildArgs;
    let events;

    // Signers
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    let addr2: SignerWithAddress;
    let addr3: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addrs: SignerWithAddress[];

    /**
     * Deploys a Remix contract with no args.
     * @param signer The signer to use.
     * @returns The Remix contract.
     */
    const deployRemixWithNoArgs = async (
        signer: Signer | Provider | string | undefined = undefined,
    ): Promise<NsRemix.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return Remix.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return Remix.deploy();
        }
    };

    /**
     * Deploy a Remix contract with args.
     * @param deployArgs The args to use.
     * @param signer The signer to use.
     * @returns The Remix contract.
     */
    const deployRemix = async (
        deployArgs: NsRemix.IsContractBuildArgs,
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemix.IsWrapper> => {
        let contract: NsRemix.IsWrapper;

        if (signer) {
            contract = await deployRemixWithNoArgs(signer);
            await contract.initialize(deployArgs);
        } else {
            contract = await deployRemixWithNoArgs();
            await contract.initialize(deployArgs);
        }

        return contract;
    };

    /**
     * Deploys a Remix Collection contract with no args.
     * @param signer The signer to use.
     * @returns The Remix Collection contract.
     */
    const deployRemixCollectionWithNoArgs = async (
        signer: Signer | Provider | string | undefined = undefined,
    ): Promise<NsRemixCollection.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixCollectionFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollection.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixCollection.deploy();
        }
    };

    /**
     * Deploy a Remix Collection contract with args.
     * @param deployArgs The args to use.
     * @param signer The signer to use.
     * @returns The Remix contract.
     */
    const deployRemixCollection = async (
        deployArgs: NsRemixCollection.IsContractBuildArgs,
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixCollection.IsWrapper> => {
        let contract: NsRemixCollection.IsWrapper;

        if (signer) {
            contract = await deployRemixCollectionWithNoArgs(signer);
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            await contract.initialize(deployArgs);
        } else {
            contract = await deployRemixCollectionWithNoArgs();
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            await contract.initialize(deployArgs);
        }

        return contract;
    };

    // TODO: Deploy an ERC20 contract

    beforeEach(async () => {
        // Get the ContractFactory and Signers here
        RemixCollection = await ethers.getContractFactory("RemixCollection") as unknown as IsRemixCollection;
        [owner, addr1, addr2, addr3, ...addrs] = await ethers.getSigners();
    });

    it("Should require the submission price to be greater than 0", async () => {
        args = remixCollectionBuildArgs();
        args[1] = BigNumber.from(0);

        await expect(deployRemixCollection(args))
            .to.be.rejectedWith("SUBMISSION_PRICE_MUST_BE_GREATER_THAN_ZERO");
    });

    it("Should require the total number of selections to be greater than 0", async () => {
        args = remixCollectionBuildArgs();
        args[2] = BigNumber.from(0);

        await expect(deployRemixCollection(args))
            .to.be.rejectedWith("TOTAL_SELECTIONS_MUST_BE_GREATER_THAN_ZERO");
    });

    it("Should deploy a Remix Collection", async () => {
        args = remixCollectionBuildArgs();
        RemixCollection1 = await deployRemixCollection(args);
    });

    it("Should emit a Deployed event when deployed", async () => {
        args = remixCollectionBuildArgs();
        RemixCollection1 = await deployRemixCollection(args);

        events = await RemixCollection1.queryFilter("Deployed");
        expect(events.length)
            .to.equal(1);
    });

    describe("open()", () => {
        beforeEach(async () => {
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);
        });

        it("Should be open at start", async () => {
            expect(await RemixCollection1.status(), "Status is not Open!")
                .to.equal(0);
        });

        it("Should revert if caller is not the owner", async () => {
            await RemixCollection1.connect(owner).close();

            await expect(RemixCollection1.connect(addr1).open())
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the contract to not be open", async () => {
            await expect(RemixCollection1.connect(owner).open())
                .to.be.rejectedWith("COLLECTION_ALREADY_OPEN");
        });

        it("Should emit an Opened event when opened", async () => {
            await RemixCollection1.connect(owner).close();
            await RemixCollection1.connect(owner).open();

            events = await RemixCollection1.queryFilter("Opened");
            expect(events.length)
                .to.equal(1);
        });
    });

    describe("close()", () => {
        beforeEach(async () => {
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixCollection1.connect(addr1).close())
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the contract to not be closed", async () => {
            await RemixCollection1.connect(owner).close();

            await expect(RemixCollection1.close())
                .to.be.rejectedWith("COLLECTION_ALREADY_CLOSED");
        });

        it("Should emit an Closed event when closed", async () => {
            await RemixCollection1.connect(owner).close();

            events = await RemixCollection1.queryFilter("Closed");
            expect(events.length).to.equal(1);
        });
    });

    describe("submit()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should require the collection to be open", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await RemixCollection1.connect(owner).close();

            await expect(RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides))
                .to.be.rejectedWith("COLLECTION_NOT_OPEN");
        });

        it("Should revert if it is not a Remix address", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await expect(RemixCollection1.connect(addr1).submit(ADDRESS_ZERO, 1, overrides))
                .to.be.rejected;
        });

        it("Should revert if a Remix is already submitted", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides);

            await expect(RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides))
                .to.be.rejectedWith("REMIX_ALREADY_SUBMITTED");
        });

        it("Should revert if the slot doesn't exists (> previous)", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await expect(RemixCollection1.connect(addr1).submit(Remix1.address, 10, overrides))
                .to.be.rejectedWith("SELECTION_SLOT_DOES_NOT_EXIST");
        });

        it("Should revert if the slot doesn't exists (< 1)", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await expect(RemixCollection1.connect(addr1).submit(Remix1.address, 0, overrides))
                .to.be.rejectedWith("SELECTION_SLOT_MUST_BE_GREATER_THAN_ZERO");
        });

        it("Should revert if the price is not high enough", async () => {
            overrides = { value: utils.parseEther("0.001") };

            await expect(RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides))
                .to.be.rejectedWith("INVALID_SUBMISSION_PRICE");
        });

        it("Should accept submissions", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides);

            expect(await RemixCollection1.isSubmitted(Remix1.address))
                .to.be.true;
            expect(await ethers.provider.getBalance(RemixCollection1.address))
                .to.equal(utils.parseEther("0.01"));
        });
    });

    describe("select()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remix
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixCollection1.connect(addr1).select(Remix1.address))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the Remix to be submitted", async () => {
            await expect(RemixCollection1.connect(owner).select(Remix1.address))
                .to.be.rejectedWith("REMIX_NOT_SUBMITTED");
        });

        it("Should push the Remix address to allSelections", async () => {
            overrides = { value: utils.parseEther("0.01") };

            await RemixCollection1.connect(addr1).submit(Remix1.address, 1, overrides);
            await RemixCollection1.connect(owner).select(Remix1.address);

            expect(await RemixCollection1.allSelections(0))
                .to.equal(Remix1.address);
        });
    });

    describe("finalize()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixCollection1.connect(addr1).finalize(ADDRESS_ZERO))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the collection to be closed", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            await expect(RemixCollection1.connect(owner).finalize(ADDRESS_ZERO))
                .to.be.rejectedWith("COLLECTION_NOT_CLOSED");
        });

        it("Should require all selection slots to be filled", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            // Missing Remix3 (slot 3)

            // Close the collection
            await RemixCollection1.connect(owner).close();

            await expect(RemixCollection1.connect(owner).finalize(ADDRESS_ZERO))
                .to.be.rejectedWith("NOT_ALL_SLOTS_HAVE_BEEN_FILLED");
        });

        it("Should revert all status-linked function if the collection has already been finalized", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            // Close & Finalize the collection
            await RemixCollection1.connect(owner).close();
            await RemixCollection1.connect(owner).finalize(ADDRESS_ZERO);

            // Try to change the status
            await expect(RemixCollection1.connect(owner).open())
                .to.be.rejectedWith("COLLECTION_IS_FINALIZED");

            await expect(RemixCollection1.connect(owner).close())
                .to.be.rejectedWith("COLLECTION_IS_FINALIZED");

            await expect(RemixCollection1.connect(owner).submit(Remix1.address, 1, overrides))
                .to.be.rejectedWith("COLLECTION_IS_FINALIZED");

            await expect(RemixCollection1.connect(owner).select(Remix1.address))
                .to.be.rejectedWith("COLLECTION_IS_FINALIZED");

            await expect(RemixCollection1.connect(owner).finalize(ADDRESS_ZERO))
                .to.be.rejectedWith("COLLECTION_IS_FINALIZED");
        });

        it("Should emit a Finalized event when collection is finalized", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            // Close the collection
            await RemixCollection1.connect(owner).close();

            await expect(RemixCollection1.connect(owner).finalize(ADDRESS_ZERO))
                .to.emit(RemixCollection1, "Finalized");
        });
    });

    describe("getWinningRemixes()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should return an empty array if the collection has not been finalized", async () => {
            expect(await RemixCollection1.getWinningRemixes())
                .to.eql([]);
        });

        it("Should return an array containing the winning remixes", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            // Close & Finalize the collection
            await RemixCollection1.connect(owner).close();
            await RemixCollection1.connect(owner).finalize(ADDRESS_ZERO);

            const winningRemixes = await RemixCollection1.getWinningRemixes();

            expect(winningRemixes[0]._remixProxyAddress, "Remix1 address is incorrect")
                .to.be.eql(Remix1.address);
            expect(winningRemixes[1]._remixProxyAddress, "Remix2 address is incorrect")
                .to.be.eql(Remix2.address);
            expect(winningRemixes[2]._remixProxyAddress, "Remix3 address is incorrect")
                .to.be.eql(Remix3.address);
        });

        it("Should return an array containing the winning remix selection numbers", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            // Close & Finalize the collection
            await RemixCollection1.connect(owner).close();
            await RemixCollection1.connect(owner).finalize(ADDRESS_ZERO);

            const winningRemixes = await RemixCollection1.getWinningRemixes();

            expect(winningRemixes[0]._selectionNumber, "Remix1 selection number is incorrect")
                .to.be.equal(1);
            expect(winningRemixes[1]._selectionNumber, "Remix2 selection number is incorrect")
                .to.be.equal(2);
            expect(winningRemixes[2]._selectionNumber, "Remix3 selection number is incorrect")
                .to.be.equal(3);
        });
    });

    describe("getSubmissionsCount()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should return 0 if no submissions have been made", async () => {
            expect(await RemixCollection1.getSubmissionsCount())
                .to.equal(0);
        });

        it("Should return the number of submissions", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            expect(await RemixCollection1.getSubmissionsCount())
                .to.equal(3);
        });
    });

    describe("getSelectionsCount()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should return 0 if no selections have been made", async () => {
            expect(await RemixCollection1.getSelectionsCount())
                .to.equal(0);
        });

        it("Should return the number of selections", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            expect(await RemixCollection1.getSelectionsCount())
                .to.equal(3);
        });
    });

    describe("getStatus()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should return 0 if the collection is open", async () => {
            expect(await RemixCollection1.getStatus())
                .to.equal(0);
        });

        it("Should return 1 if the collection is closed", async () => {
            await RemixCollection1.connect(owner).close();

            expect(await RemixCollection1.getStatus())
                .to.equal(1);
        });

        it("Should return 2 if the collection is finalized", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);
            await RemixCollection1.submit(Remix2.address, 2, overrides);
            await RemixCollection1.submit(Remix3.address, 3, overrides);

            // Select Remixes
            await RemixCollection1.connect(owner).select(Remix1.address);
            await RemixCollection1.connect(owner).select(Remix2.address);
            await RemixCollection1.connect(owner).select(Remix3.address);

            // Close & finalize the collection
            await RemixCollection1.connect(owner).close();
            await RemixCollection1.connect(owner).finalize(ADDRESS_ZERO);

            expect(await RemixCollection1.getStatus())
                .to.equal(2);
        });
    });

    describe("isSubmitted()", () => {
        beforeEach(async () => {
            // Get the ContractFactory
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remix Collection
            args = remixCollectionBuildArgs();
            RemixCollection1 = await deployRemixCollection(args);

            // Deploy Remixes
            args = remixBuildArgs([addr1.address]);
            Remix1 = await deployRemix(args);
            args = remixBuildArgs([addr2.address]);
            Remix2 = await deployRemix(args);
            args = remixBuildArgs([addr3.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should return false if the remix has not been submitted", async () => {
            expect(await RemixCollection1.isSubmitted(Remix1.address))
                .to.equal(false);
        });

        it("Should return true if the remix has been submitted", async () => {
            overrides = { value: utils.parseEther("0.01") };

            // Submit Remixes
            await RemixCollection1.submit(Remix1.address, 1, overrides);

            expect(await RemixCollection1.isSubmitted(Remix1.address))
                .to.equal(true);
        });
    });
});