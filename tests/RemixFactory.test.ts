import { Provider } from "@ethersproject/abstract-provider/src.ts/index";
import { Signer } from "@ethersproject/abstract-signer/src.ts/index";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { BigNumber } from "ethers";
import { ethers } from "hardhat";

import { ADDRESS_ZERO, remixBuildArgs } from "helpers/tests";
import NsRemixFactory from "types/RemixFactory";
import NsRemixUpgradeTest from "types/tests/RemixUpgradeTest";

import { Remix as IsRemix } from "../typechain-types/contracts/Remix.sol/Remix";
import { RemixFactory as IsRemixFactory } from "../typechain-types/contracts/RemixFactory.sol/RemixFactory";
import { RemixUpgradeTest as IsRemixUpgradeTest } from "../typechain-types/contracts/tests/RemixUpgradeTest";


describe("RemixFactory", () => {
    // Remix Factory
    let RemixFactory: IsRemixFactory;
    let RemixFactory1: NsRemixFactory.IsWrapper;

    // Remix upgrade test
    let RemixUpgradeTest: IsRemixUpgradeTest;
    let RemixUpgradeTest1: NsRemixUpgradeTest.IsWrapper;

    let overrides: NsRemixFactory.IsPayableOverrides;
    let args: NsRemixFactory.IsContractBuildArgs;
    let events;

    // Signers
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addr2: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addrs: SignerWithAddress[];

    /**
     * Deploys a RemixFactory contract.
     * @param signer The signer to use.
     * @param factoryCut The factory cut (defaults to 5%).
     * @returns The RemixFactory contract.
     */
    const deployRemixFactory = async (
        signer: Signer | Provider | string | undefined = undefined,
        factoryCut = BigNumber.from(500)
    ): Promise<NsRemixFactory.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixFactory.connect(signer).deploy(factoryCut);
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixFactory.deploy(factoryCut);
        }
    };

    /**
     * Deploys a Remix contract from a RemixFactory contract.
     * With the owner address as the contract owner.
     * @param factory The RemixFactory contract.
     * @param authors The authors of the contract.
     * @returns The Remix proxy contract address.
     */
    const deployRemixFromFactory = async (
        factory: NsRemixFactory.IsWrapper,
        authors: string[]
    ): Promise<string> => {
        args = remixBuildArgs(authors);

        const tx = await factory.deploy(args);
        const receipt = await tx.wait();

        const event = receipt.events?.find((e) => e.event === "RemixDeployed");
        const proxyAddress = event?.args?.remixProxyAddress;

        return proxyAddress as string;
    };

    /**
     * Deploys a RemixUpgradeTest contract with no args.
     * @param signer The signer to use.
     * @returns The Remix implementation contract.
     */
    const deployRemixUpgradeTestWithNoArgs = async (
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixUpgradeTest.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixUpgradeTest.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return RemixUpgradeTest.deploy();
        }
    };

    /**
     * Deploy a Remix implementation contract with args.
     * @param deployArgs The args to use.
     * @param signer The signer to use.
     * @returns The Remix implementation contract.
     */
    const deployRemixUpgradeTest = async (
        deployArgs: NsRemixUpgradeTest.IsContractBuildArgs,
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemixUpgradeTest.IsWrapper> => {
        let contract: NsRemixUpgradeTest.IsWrapper;

        if (signer) {
            contract = await deployRemixUpgradeTestWithNoArgs(signer);
            await contract.initialize(deployArgs);
        } else {
            contract = await deployRemixUpgradeTestWithNoArgs();
            await contract.initialize(deployArgs);
        }

        return contract;
    };

    // TODO: Deploy an ERC20 contract

    beforeEach(async () => {
        // Get the ContractFactory and Signers here
        RemixFactory = await ethers.getContractFactory("RemixFactory") as unknown as IsRemixFactory;
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    });

    it("Should require the factoryCut to be less than 10000", async () => {
        await expect(deployRemixFactory(undefined, BigNumber.from(10000)))
            .to.be.rejectedWith("INVALID_CUT");
    });

    it("Should deploy a RemixFactory", async () => {
        RemixFactory1 = await deployRemixFactory();
    });

    it("Should deploy a RemixFactory and emit a Deployed event", async () => {
        RemixFactory1 = await deployRemixFactory();

        events = await RemixFactory1.queryFilter("Deployed");
        expect(events, "Deployed event missing")
            .to.have.length(1);
    });

    it("Should deploy a RemixFactory and emit an unique Deployed event", async () => {
        RemixFactory1 = await deployRemixFactory();

        events = await RemixFactory1.queryFilter("Deployed");
        expect(events.length, "Deployed event number is not correct")
            .to.eql(1);
    });

    it("Should set the owner of the RemixFactory", async () => {
        RemixFactory1 = await deployRemixFactory();

        expect(await RemixFactory1.owner(), "Owner is not correct")
            .to.eql(owner.address);
    });

    it("Should set the owner of the RemixFactory as admin (AccessControl)", async () => {
        RemixFactory1 = await deployRemixFactory();
        const adminRoleKeccak = await RemixFactory1.DEFAULT_ADMIN_ROLE();

        expect(await RemixFactory1.hasRole(adminRoleKeccak, owner.address), "Owner is not admin")
            .to.eql(true);
    });

    describe("deploy()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            RemixFactory = await ethers.getContractFactory("RemixFactory") as unknown as IsRemixFactory;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

            RemixFactory1 = await deployRemixFactory();
        });

        it("Should require the owner to be one of the authors of the Remix", async () => {
            args = remixBuildArgs([addr1.address]);

            await expect(RemixFactory1.deploy(args))
                .to.be.rejectedWith("SENDER_IS_NOT_AUTHOR");
        });

        it("Should register the Remix and emit a RemixRegistered event", async () => {
            args = remixBuildArgs([owner.address]);
            await RemixFactory1.deploy(args);

            events = await RemixFactory1.queryFilter("RemixRegistered");
            expect(events, "RemixRegistered event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixDeployed event", async () => {
            args = remixBuildArgs([owner.address]);
            await RemixFactory1.deploy(args);

            events = await RemixFactory1.queryFilter("RemixDeployed");
            expect(events, "RemixDeployed event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixDeployed event that contains the authors, the proxy and the implementation addresses", async () => {
            args = remixBuildArgs([owner.address]);
            await RemixFactory1.deploy(args);

            events = await RemixFactory1.queryFilter("RemixDeployed");
            expect(events[0].args.authors, "Author addresses are not correct")
                .to.eql([owner.address]);
            expect(events[0].args.remixProxyAddress, "Proxy address is not correct")
                .to.not.be.undefined;
            expect(events[0].args.remixImplementation, "Implementation address is not correct")
                .to.not.be.undefined;
        });

        it("Should return the proxy address", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            expect(proxyAddress, "Proxy address is not correct")
                .to.not.be.undefined;
        });

        it("Should return the correct proxy address (checked by calling a public Remix function)", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Calling getAuthors() to check contract interaction with the proxy
            expect(await RemixProxy.getAuthors(), "Authors are not correct").
                to.be.eql([owner.address]);
        });

        it("Should have the correct factory address (Remix -> factory public var)", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            const factoryAddressFromProxy = await RemixProxy.factory();
            expect(factoryAddressFromProxy, "Factory address is not correct")
                .to.eql(RemixFactory1.address);
        });
    });

    describe("registerRemix()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            RemixFactory = await ethers.getContractFactory("RemixFactory") as unknown as IsRemixFactory;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

            RemixFactory1 = await deployRemixFactory();
        });

        it("Should require the address to be a Remix contract", async () => {
            await expect(RemixFactory1.registerRemix(addr1.address))
                .to.be.rejected;
        });

        it("Should add the Remix to the list of registered Remixes (registry)", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            expect(await RemixFactory1.getRemixByAuthor(owner.address), "Remix not registered")
                .to.eql([proxyAddress]);
        });

        it("Should emit a RemixRegistered event", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            await expect(RemixFactory1.registerRemix(proxyAddress))
                .to.emit(RemixFactory1, "RemixRegistered");
        });
    });

    describe("upgradeRemixImplementation()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();

            // Get the ContractFactory here.
            RemixUpgradeTest = await ethers.getContractFactory("RemixUpgradeTest") as unknown as IsRemixUpgradeTest;
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixFactory1.connect(addr1).upgradeRemixImplementation(ADDRESS_ZERO))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("(INTERNAL VERIFICATION) Should deploy a new Remix implementation contract based on RemixUpgradeTest", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            expect(RemixUpgradeTest1.address, "Implementation address is not correct")
                .to.not.be.undefined;
        });

        it("(INTERNAL VERIFICATION) Should deploy an extension of the Remix contract", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            expect(await RemixUpgradeTest1.getAuthors(), "Authors are not correct")
                .to.eql([owner.address]);
        });

        it("Should revert if the implementation address is address(0)", async () => {
            await expect(RemixFactory1.upgradeRemixImplementation(ADDRESS_ZERO))
                .to.be.rejectedWith("NEW_IMPLEMENTATION_ADDRESS_CANNOT_BE_ZERO");
        });

        it("Should revert if the implementation address is the same as the current one", async () => {
            const currentImplementationAddress = await RemixFactory1.getRemixImplementation();

            await expect(RemixFactory1.upgradeRemixImplementation(currentImplementationAddress))
                .to.be.rejectedWith("IMPLEMENTATION_ADDRESSES_CANNOT_BE_SAME");
        });

        it("Should change the Remix implementation address", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            const newImplementationAddress = await RemixFactory1.getRemixImplementation();
            expect(newImplementationAddress, "Implementation address is not correct")
                .to.eql(RemixUpgradeTest1.address);
        });

        it("Should emit a RemixImplementationUpgraded event", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            events = await RemixFactory1.queryFilter("RemixImplementationUpgraded");
            expect(events, "RemixImplementationUpgraded event missing")
                .to.have.length(1);
        });

        it("Should emit a RemixImplementationUpgraded event that contains the previous implementation address", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            events = await RemixFactory1.queryFilter("RemixImplementationUpgraded");
            expect(events[0].args.prevRemixImplementation, "Previous implementation address is not correct")
                .to.not.be.undefined;
        });

        it("Should emit a RemixImplementationUpgraded event that contains the new implementation address", async () => {
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            events = await RemixFactory1.queryFilter("RemixImplementationUpgraded");
            expect(events[0].args.newRemixImplementation, "New implementation address is not correct")
                .to.eql(RemixUpgradeTest1.address);
        });

        it("Should allow interactions with a proxy deployed after an implementation address update", async () => {
            // Deploy the upgrade test contract for the new Remix implementation
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            // Upgrade the Remix implementation
            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            // Deploy a new proxy
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Calling getAuthors() to check contract interaction with the proxy
            expect(await RemixProxy.getAuthors(), "Authors are not correct").
                to.be.eql([owner.address]);
        });

        it("Should upgrade the previously deployed proxies and allow to call new functions on them", async () => {
            // Deploy a Remix proxy
            args = remixBuildArgs([owner.address]);
            const proxyAddress1 = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Deploy the upgrade test contract for the new Remix implementation
            args = remixBuildArgs([owner.address]);
            RemixUpgradeTest1 = await deployRemixUpgradeTest(args);

            // Upgrade the Remix implementation
            await RemixFactory1.upgradeRemixImplementation(RemixUpgradeTest1.address);

            // Getting the complete Remix contract (with the new implementation)
            const newRemixProxy = RemixUpgradeTest.attach(proxyAddress1) as unknown as IsRemixUpgradeTest;

            // Calling testUpgrade() to check contract interaction with the proxy
            expect(await newRemixProxy.testUpgrade(), "Test upgrade is not correct").
                to.be.eql("testUpgrade");
        });
    });

    describe("harvestMany()", () => {
        let proxy1: IsRemix;
        let proxy2: IsRemix;

        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();

            args = remixBuildArgs([owner.address]);
            const proxyAddress1 = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            args = remixBuildArgs([owner.address]);
            const proxyAddress2 = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            proxy1 = RemixProxyObj.attach(proxyAddress1) as unknown as IsRemix;
            proxy2 = RemixProxyObj.attach(proxyAddress2) as unknown as IsRemix;

            overrides = { value: ethers.utils.parseEther("1") };

            // Purchase the collectibles with addr1
            await proxy1.connect(addr1).purchaseCollectible(overrides);
            await proxy2.connect(addr1).purchaseCollectible(overrides);
        });

        it("Should allow addresses to harvest many Remixes", async () => {
            await RemixFactory1.connect(owner).harvestMany([proxy1.address, proxy2.address], ADDRESS_ZERO);
        });

        it("Should also send 5% of the royalties to the RemixFactory", async () => {
            const factoryBalanceBefore = await ethers.provider.getBalance(RemixFactory1.address);
            await RemixFactory1.connect(owner).harvestMany([proxy1.address, proxy2.address], ADDRESS_ZERO);
            const factoryBalanceAfter = await ethers.provider.getBalance(RemixFactory1.address);

            expect(factoryBalanceAfter.sub(factoryBalanceBefore), "Factory balance is not correct")
                .to.be.gt(BigNumber.from(0));
        });
    });

    describe("harvestFactory()", () => {
        const factoryCut = BigNumber.from(500);

        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory(undefined, factoryCut);
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixFactory1.connect(addr1).harvestFactory(ADDRESS_ZERO, owner.address))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the '_to' address to not be the zero address", async () => {
            await expect(RemixFactory1.harvestFactory(ADDRESS_ZERO, ADDRESS_ZERO))
                .to.be.rejectedWith("INVALID_HARVEST_ADDRESS");
        });

        it("Should revert if the RemixFactory has no royalties to harvest", async () => {
            await expect(RemixFactory1.harvestFactory(ADDRESS_ZERO, owner.address))
                .to.be.rejectedWith("NO_ETHER_TO_HARVEST");
        });

        it("Should set the factory cut of the Remix to 5%, recovered from the factory", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Buys a collectible to have some royalties to harvest
            overrides = { value: ethers.utils.parseEther("0.1") };
            await RemixProxy.connect(addr2).purchaseCollectible(overrides);

            // Harvests the royalties (from the Remix contract)
            // Which should also send 5% to the RemixFactory
            await RemixProxy.connect(owner).harvestRoyalties(ADDRESS_ZERO);

            expect(await RemixProxy.factoryCut(), "Factory cut is not correct")
                .to.be.equal(BigNumber.from(factoryCut));
        });

        it("Should harvest the factory and send the ETH to the '_to' address", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Buys a collectible to have some royalties to harvest
            overrides = { value: ethers.utils.parseEther("0.1") };
            await RemixProxy.connect(addr2).purchaseCollectible(overrides);

            // Harvests the royalties (from the Remix contract)
            // Which should also send 5% to the RemixFactory
            await RemixProxy.connect(owner).harvestRoyalties(ADDRESS_ZERO);

            // Getting the previous owner balance
            const previousOwnerBalance = await ethers.provider.getBalance(owner.address);

            // Get the previous balance of the factory
            const previousFactoryBalance = await ethers.provider.getBalance(RemixFactory1.address);

            // Harvests the royalties (from the RemixFactory contract)
            await RemixFactory1.harvestFactory(ADDRESS_ZERO, owner.address);

            // Getting the new owner balance
            const newOwnerBalance = await ethers.provider.getBalance(owner.address);

            // Get the new balance of the factory
            const newFactoryBalance = await ethers.provider.getBalance(RemixFactory1.address);

            expect(previousFactoryBalance, "Previous factory balance is not correct")
                .to.be.gt(BigNumber.from(0));
            expect(newFactoryBalance, "New factory balance is not correct")
                .to.be.equal(0);
            expect(newOwnerBalance.sub(previousOwnerBalance), "Owner balance is not correct")
                .to.be.gt(BigNumber.from(0));
        });

        it("Should harvest the factory and emit a Harvested event", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Buys a collectible to have some royalties to harvest
            overrides = { value: ethers.utils.parseEther("0.1") };
            await RemixProxy.connect(addr2).purchaseCollectible(overrides);

            // Harvests the royalties (from the Remix contract)
            // Which should also send 5% to the RemixFactory
            await RemixProxy.connect(owner).harvestRoyalties(ADDRESS_ZERO);

            // Harvests the royalties (from the RemixFactory contract)
            await RemixFactory1.harvestFactory(ADDRESS_ZERO, owner.address);

            // Getting the Harvested event
            events = await RemixFactory1.queryFilter("Harvested");
            expect(events, "Harvested event missing")
                .to.have.length(1);
        });

        it("Should emit a Harvested event that contains the proper values", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Getting the complete Remix contract
            const RemixProxyObj = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            const RemixProxy = RemixProxyObj.attach(proxyAddress) as unknown as IsRemix;

            // Buys a collectible to have some royalties to harvest
            overrides = { value: ethers.utils.parseEther("0.1") };
            await RemixProxy.connect(addr2).purchaseCollectible(overrides);

            // Harvests the royalties (from the Remix contract)
            // Which should also send 5% to the RemixFactory
            await RemixProxy.connect(owner).harvestRoyalties(ADDRESS_ZERO);

            // Get the balance of the factory
            const factoryBalance = await ethers.provider.getBalance(RemixFactory1.address);

            // Harvests the royalties (from the RemixFactory contract)
            await RemixFactory1.harvestFactory(ADDRESS_ZERO, owner.address);

            // Getting the RemixFactoryRoyaltiesHarvested event
            events = await RemixFactory1.queryFilter("Harvested");
            expect(events[0].args.to, "'to' address is not correct")
                .to.eql(owner.address);
            expect(events[0].args.tokenAddress, "'tokenAddress' is not correct")
                .to.eql(ADDRESS_ZERO);
            expect(events[0].args.amount, "'amount' is not correct")
                .to.equal(factoryBalance);
        });
    });

    describe("updateFactoryCut()", () => {
        const factoryCut = BigNumber.from(500);

        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory(undefined, factoryCut);
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixFactory1.connect(addr1).updateFactoryCut(1000))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should require the factoryCut to be less than 10000", async () => {
            await expect(RemixFactory1.connect(owner).updateFactoryCut(10000))
                .to.be.rejectedWith("INVALID_CUT");
        });

        it("Should emit a CutUpdated event", async () => {
            await RemixFactory1.connect(owner).updateFactoryCut(1000);

            events = await RemixFactory1.queryFilter("CutUpdated");
            expect(events, "CutUpdated event missing")
                .to.have.length(1);
        });

        it("Should emit a CutUpdated event that contains the proper values", async () => {
            await RemixFactory1.connect(owner).updateFactoryCut(1000);

            events = await RemixFactory1.queryFilter("CutUpdated");
            expect(events[0].args.previousFactoryCut, "'previousFactoryCut' is not correct")
                .to.eql(factoryCut);
            expect(events[0].args.newFactoryCut, "'newFactoryCut' is not correct")
                .to.equal(1000);
        });
    });

    describe("flag()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should revert if caller is not a member of the staff (admin/moderator)", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            await expect(RemixFactory1.connect(addr1).flag(proxyAddress))
                .to.be.rejectedWith("NOT_MEMBER_OF_STAFF");
        });

        it("Should allow a member of the staff (admin) to flag a Remix", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            await RemixFactory1.connect(owner).flag(proxyAddress);
        });

        it("Should allow a member of the staff (moderator) to flag a Remix", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            await RemixFactory1.connect(addr1).flag(proxyAddress);
        });
    });

    describe("unflag()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should revert if caller is not a member of the staff (admin/moderator)", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Flagging the Remix
            await RemixFactory1.connect(owner).flag(proxyAddress);

            await expect(RemixFactory1.connect(addr1).unflag(proxyAddress, 0))
                .to.be.rejectedWith("NOT_MEMBER_OF_STAFF");
        });

        it("Should allow a member of the staff (admin) to unflag a Remix", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Flagging the Remix first
            await RemixFactory1.connect(owner).flag(proxyAddress);

            await RemixFactory1.connect(owner).unflag(proxyAddress, 0);
        });

        it("Should allow a member of the staff (moderator) to unflag a Remix", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            // Flagging the Remix first
            await RemixFactory1.connect(owner).flag(proxyAddress);

            await RemixFactory1.connect(addr1).unflag(proxyAddress, 0);
        });

        it("Should revert if the Remix is not flagged", async () => {
            args = remixBuildArgs([owner.address]);
            const proxyAddress = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            await expect(RemixFactory1.connect(owner).unflag(proxyAddress, 0))
                .to.be.rejectedWith("NOT_FLAGGED");
        });
    });

    describe("getRemixImplementation()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should return the same Remix implementation address as the RemixDeployed event", async () => {
            args = remixBuildArgs([owner.address]);
            await RemixFactory1.deploy(args);

            events = await RemixFactory1.queryFilter("RemixDeployed");
            expect(await RemixFactory1.getRemixImplementation(), "Implementation address is not correct")
                .to.eql(events[0].args.remixImplementation);
        });
    });

    describe("getRemixByAuthor()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should return the correct number of Remix proxy address(es)", async () => {
            args = remixBuildArgs([owner.address]);

            // Deploying 2 Remixes
            await RemixFactory1.deploy(args);
            await RemixFactory1.deploy(args);

            expect(await RemixFactory1.getRemixByAuthor(owner.address), "Remix proxy address(es) number is not correct")
                .to.have.length(2);
        });

        it("Should return the correct Remix proxy address(es)", async () => {
            args = remixBuildArgs([owner.address]);

            // Deploying 2 Remixes and getting their addresses
            const proxyAddress1 = await deployRemixFromFactory(RemixFactory1, [owner.address]);
            const proxyAddress2 = await deployRemixFromFactory(RemixFactory1, [owner.address]);

            expect(await RemixFactory1.getRemixByAuthor(owner.address), "Remix proxy address(es) is not correct")
                .to.have.members([proxyAddress1, proxyAddress2]);
        });
    });

    describe("addOrRevokeModerator()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should revert if caller is not the owner", async () => {
            await expect(RemixFactory1.connect(addr1).addOrRevokeModerator(addr1.address))
                .to.be.rejectedWith("Ownable: caller is not the owner");
        });

        it("Should add a moderator and emit a ModeratorAdded event", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixFactory1.queryFilter("ModeratorAdded");
            expect(events, "ModeratorAdded event missing")
                .to.have.length(1);
        });

        it("Should add a moderator and emit a ModeratorAdded event that contains the added moderator address", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixFactory1.queryFilter("ModeratorAdded");
            expect(events[0].args.account, "Moderator address is not correct")
                .to.eql(addr1.address);
        });

        it("Should revoke a moderator and emit a ModeratorRevoked event", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixFactory1.queryFilter("ModeratorRevoked");
            expect(events, "ModeratorRevoked event missing")
                .to.have.length(1);
        });

        it("Should revoke a moderator and emit a ModeratorRevoked event that contains the revoked moderator address", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            events = await RemixFactory1.queryFilter("ModeratorRevoked");
            expect(events[0].args.account, "Moderator address is not correct")
                .to.eql(addr1.address);
        });
    });

    describe("hasModeratorRole()", () => {
        beforeEach(async () => {
            RemixFactory1 = await deployRemixFactory();
        });

        it("Should return true if the address has the moderator role", async () => {
            await RemixFactory1.connect(owner).addOrRevokeModerator(addr1.address);

            expect(await RemixFactory1.hasModeratorRole(addr1.address), "Not moderator but should be")
                .to.eql(true);
        });

        it("Should return false if the address does not have the moderator role", async () => {
            expect(await RemixFactory1.hasModeratorRole(addr1.address), "Moderator but should not be")
                .to.eql(false);
        });
    });
});