import { Provider } from "@ethersproject/abstract-provider/src.ts/index";
import { Signer } from "@ethersproject/abstract-signer/src.ts/index";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { BigNumber, utils } from "ethers";
import { ethers } from "hardhat";

import { ADDRESS_ZERO, remixBuildArgs } from "helpers/tests";
import NsRemix from "types/Remix";

import { Remix as IsRemix } from "../typechain-types/contracts/Remix.sol/Remix";


describe("Remix", () => {
    // Remix
    let Remix: IsRemix;
    let Remix1: NsRemix.IsWrapper;
    let Remix2: NsRemix.IsWrapper;
    let Remix3: NsRemix.IsWrapper;

    let overrides: NsRemix.IsPayableOverrides;
    let args: NsRemix.IsContractBuildArgs;
    let events;

    // Signers
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    let addr2: SignerWithAddress;
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    let addrs: SignerWithAddress[];

    /**
     * Deploys a Remix contract with no args.
     * @param signer The signer to use.
     * @returns The Remix contract.
     */
    const deployRemixWithNoArgs = async (
        signer: Signer | Provider | string | undefined = undefined,
    ): Promise<NsRemix.IsWrapper> => {
        /**
         * Quick note: the "deploy()" here should not be mistaken for the "deploy()" function
         * in the RemixFactory contract. This function here is the Hardhat function that
         * deploys a contract.
         */

        if (signer) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return Remix.connect(signer).deploy();
        } else {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return Remix.deploy();
        }
    };

    /**
     * Deploy a Remix contract with args.
     * @param deployArgs The args to use.
     * @param signer The signer to use.
     * @returns The Remix contract.
     */
    const deployRemix = async (
        deployArgs: NsRemix.IsContractBuildArgs,
        signer: Signer | Provider | string | undefined = undefined
    ): Promise<NsRemix.IsWrapper> => {
        let contract: NsRemix.IsWrapper;

        if (signer) {
            contract = await deployRemixWithNoArgs(signer);
            await contract.initialize(deployArgs);
        } else {
            contract = await deployRemixWithNoArgs();
            await contract.initialize(deployArgs);
        }

        return contract;
    };

    // TODO: Deploy an ERC20 contract

    beforeEach(async () => {
        // Get the ContractFactory and Signers here
        Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    });

    it("Should require at least one author", async () => {
        args = remixBuildArgs([]);

        Remix1 = await deployRemixWithNoArgs();
        await expect(Remix1.initialize(args), "Should require at least one author")
            .to.be.rejectedWith("MISSING_AUTHORS");
    });

    it("Should require authors split to be the same length as authors", async () => {
        args = remixBuildArgs([owner.address]);
        args[2] = [2000, 2000];

        Remix1 = await deployRemixWithNoArgs();
        await expect(Remix1.initialize(args), "Authors split should be the same length as authors")
            .to.be.rejectedWith("AUTHORS_LEN_NOT_EQUAL_TO_AUTHOR_SPLITS_LEN");
    });

    it("Should require parents split to be the same length as parents", async () => {
        args = remixBuildArgs([owner.address], [owner.address]);
        args[4] = [];

        Remix1 = await deployRemixWithNoArgs();
        await expect(Remix1.initialize(args), "Parents split should be the same length as parents")
            .to.be.rejectedWith("PARENTS_LEN_NOT_EQUAL_TO_PARENT_SPLITS_LEN");
    });

    it("Should require royalties to be less than 10000", async () => {
        args = remixBuildArgs([owner.address]);
        args[10] = 10001;

        Remix1 = await deployRemixWithNoArgs();
        await expect(Remix1.initialize(args), "Royalties should be less than 10000")
            .to.be.rejectedWith("EXCEEDS_MAX_ROYALTIES");
    });

    it("Should deploy a Remix contract with no parents", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemix(args);
    });

    it("Should deploy a Remix contract with multiple authors", async () => {
        args = remixBuildArgs([owner.address, addr1.address]);
        Remix1 = await deployRemix(args);
    });

    it("Should deploy a Remix with parents", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemix(args, owner);

        args = remixBuildArgs([owner.address], [Remix1.address]);
        Remix2 = await deployRemix(args, owner);

        const parents = await Remix2.getParents();
        expect(parents, "Contract did not save the parents")
            .to.eql([Remix1.address]);
    });

    it("Should deploy a Remix with parents and emit a DerivativeIssued event", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemix(args, owner);

        args = remixBuildArgs([owner.address], [Remix1.address]);
        Remix2 = await deployRemix(args, owner);

        events = await Remix1.queryFilter("DerivativeIssued");
        expect(events, "DerivativeIssue event missing")
            .to.have.length(1);
    });

    it("Should emit a Deployed event on initialize", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemixWithNoArgs();

        await expect(Remix1.initialize(args), "Did not emit Deployed event!")
            .to.emit(Remix1, "Deployed")
            .withArgs([owner.address], true);
    });

    it("Should emit an unique Deployed event on initialize", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemixWithNoArgs();
        await Remix1.initialize(args);

        events = await Remix1.queryFilter("Deployed");
        expect(events.length, "Deployed event number is not correct")
            .to.eql(1);
    });

    it("Should require the RMX token", async () => {
        args = remixBuildArgs([owner.address]);
        Remix1 = await deployRemix(args);

        args = remixBuildArgs([addr1.address], [Remix1.address]);

        Remix2 = await deployRemixWithNoArgs(addr1);
        await expect(Remix2.connect(addr1).initialize(args), "Should require the RMX token (addr1)")
            .to.be.rejectedWith("MISSING_AUTHOR_LICENSE");

        Remix2 = await deployRemixWithNoArgs(addr1);
        await expect(Remix2.connect(owner).initialize(args), "Should require the RMX token (owner)")
            .to.be.rejectedWith("MISSING_AUTHOR_LICENSE");

        overrides = { value: utils.parseEther("0.01") };

        await Remix1.connect(addr1).purchaseRMX(overrides);

        Remix2 = await deployRemixWithNoArgs(addr1);
        await Remix2.connect(owner).initialize(args);
    });

    describe("purchaseRMX()", async () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        });

        it("Should require the msg.value to be greater or equal to the min purchase price", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.001") };
            await expect(Remix1.connect(addr1).purchaseRMX(overrides), "Invalid purchase price not reverted")
                .to.be.rejectedWith("INVALID_RMX_PURCHASE_PRICE");
        });

        it("Should require the buyer to not already own the RMX", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.01") };
            await Remix1.connect(addr1).purchaseRMX(overrides);

            overrides = { value: utils.parseEther("0.06") };
            await expect(Remix1.connect(addr1).purchaseRMX(overrides), "Buyer already owns the RMX not reverted")
                .to.be.rejectedWith("RMX_BUYER_IS_ALREADY_OWNER");
        });

        it("Should emit a RMXPurchased event on buying", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.01") };
            await expect(Remix1.connect(addr1).purchaseRMX(overrides), "Did not emit RMXPurchased event!")
                .to.emit(Remix1, "RMXPurchased")
                .withArgs(addr1.address, utils.parseEther("0.01"));
        });

        it("Should purchase the RMX", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.01") };
            await Remix1.connect(addr1).purchaseRMX(overrides);
        });
    });

    describe("purchaseCollectible()", async () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        });

        it("Should require the total supply for the collectible to be zero", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.1") };
            await Remix1.connect(addr1).purchaseCollectible(overrides);

            await expect(Remix1.connect(addr1).purchaseCollectible(overrides), "Total supply not zero not reverted")
                .to.be.rejectedWith("COLLECTIBLE_ALREADY_MINTED_OR_PURCHASED");
        });

        it("Should require the msg.value to be greater or equal to the collectible price", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.001") };
            await expect(Remix1.connect(addr1).purchaseCollectible(overrides), "Invalid purchase price not reverted")
                .to.be.rejectedWith("INVALID_COLLECTIBLE_PURCHASE_PRICE");
        });

        it("Should emit a CollectiblePurchased event on buying", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.1") };
            await expect(Remix1.connect(addr1).purchaseCollectible(overrides), "Did not emit CollectiblePurchased event!")
                .to.emit(Remix1, "CollectiblePurchased")
                .withArgs(addr1.address, utils.parseEther("0.1"));
        });

        it("Should purchase the collectible", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.1") };
            await Remix1.connect(addr1).purchaseCollectible(overrides);
        });
    });

    describe("purchaseUsage()", async () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        });

        it("Should require the msg.value to be greater or equal to the usage price", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.001") };
            await expect(Remix1.connect(addr1).purchaseUsage(overrides), "Invalid purchase price not reverted")
                .to.be.rejectedWith("INVALID_USAGE_PURCHASE_PRICE");
        });

        it("Should require the buyer to not already own the usage", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.05") };
            await Remix1.connect(addr1).purchaseUsage(overrides);

            overrides = { value: utils.parseEther("0.05") };
            await expect(Remix1.connect(addr1).purchaseUsage(overrides), "Buyer already owns the usage not reverted")
                .to.be.rejectedWith("ALREADY_PURCHASED_USAGE_RIGHTS");
        });

        it("Should emit a UsagePurchased event on buying", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.05") };
            await expect(Remix1.connect(addr1).purchaseUsage(overrides), "Did not emit UsagePurchased event!")
                .to.emit(Remix1, "UsagePurchased")
                .withArgs(addr1.address, utils.parseEther("0.05"));
        });

        it("Should purchase the usage", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.05") };
            await Remix1.connect(addr1).purchaseUsage(overrides);
        });
    });

    describe("harvestRoyalties()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address], [Remix1.address]);
            Remix2 = await deployRemix(args);

            args = remixBuildArgs([owner.address, addr1.address], [Remix1.address, Remix2.address]);
            Remix3 = await deployRemix(args);

            overrides = { value: utils.parseEther("0.2") };
            await Remix1.connect(addr2).purchaseCollectible(overrides);
            await Remix2.connect(addr2).purchaseCollectible(overrides);
            await Remix3.connect(addr2).purchaseCollectible(overrides);
        });

        it("Should have the initial factory cut to 0", async () => {
            expect(await Remix1.factoryCut(), "Factory cut is not 0")
                .to.equal(0);
        });

        it("Should have ETH in contracts", async () => {
            expect(await ethers.provider.getBalance(Remix1.address), "Remix1 does not have funds")
                .to.equal(utils.parseEther("0.2"));
            expect(await ethers.provider.getBalance(Remix2.address), "Remix2 does not have funds")
                .to.equal(utils.parseEther("0.2"));
            expect(await ethers.provider.getBalance(Remix3.address), "Remix3 does not have funds")
                .to.equal(utils.parseEther("0.2"));
        });

        it("Should get a factory cut of 0% because of a non existent factory", async () => {
            await Remix1.connect(addr1).harvestRoyalties(ADDRESS_ZERO);

            expect(await Remix1.factoryCut(), "Factory cut is not 0")
                .to.equal(0);
        });

        it("Should harvest to one author", async () => {
            const royalties = await ethers.provider.getBalance(Remix1.address);

            const before = await ethers.provider.getBalance(owner.address);
            await Remix1.connect(addr1).harvestRoyalties(ADDRESS_ZERO);
            const after = await ethers.provider.getBalance(owner.address);

            expect(after.sub(before), "Owner did not receive funds")
                .to.equal(royalties);
        });

        it("Should harvest to multiple authors", async () => {
            const royalties = await ethers.provider.getBalance(Remix3.address);

            const beforeOwner = await ethers.provider.getBalance(owner.address);
            const beforeAddr1 = await ethers.provider.getBalance(addr1.address);
            await Remix3.connect(addr2).harvestRoyalties(ADDRESS_ZERO);
            const afterOwner = await ethers.provider.getBalance(owner.address);
            const afterAddr1 = await ethers.provider.getBalance(addr1.address);

            expect(afterOwner.sub(beforeOwner), "Owner did not receive funds")
                .to.equal(royalties.div(4));
            expect(afterAddr1.sub(beforeAddr1), "Addr1 did not receive funds")
                .to.equal(royalties.div(4));
        });

        it("Should harvest and send up the chain once", async () => {
            const royalties = await ethers.provider.getBalance(Remix1.address);

            const before = await ethers.provider.getBalance(Remix1.address);
            await Remix2.harvestRoyalties(ADDRESS_ZERO);
            const after = await ethers.provider.getBalance(Remix1.address);

            expect(after.sub(before), "Remix1 did not receive funds")
                .to.equal(royalties.div(2));
        });

        it("Should harvest and send up the chain to all parents", async () => {
            const royalties = await ethers.provider.getBalance(Remix3.address);

            const beforeRemix1 = await ethers.provider.getBalance(Remix1.address);
            const beforeRemix2 = await ethers.provider.getBalance(Remix2.address);
            await Remix3.harvestRoyalties(ADDRESS_ZERO);
            const afterRemix1 = await ethers.provider.getBalance(Remix1.address);
            const afterRemix2 = await ethers.provider.getBalance(Remix1.address);

            expect(afterRemix1.sub(beforeRemix1), "Remix1 did not receive funds")
                .to.equal(royalties.div(4));
            expect(afterRemix2.sub(beforeRemix2), "Remix2 did not receive funds")
                .to.equal(royalties.div(4));
        });

        it("Should work for ERC20", async () => {
            // TODO: Deploy an ERC20 contract
            throw Error("Must be implemented");
        });
    });

    describe("requestDerivative()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        });

        it("Should revert if sender does not support IRemix interface", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            const authors = await Remix1.connect(owner).getAuthors();

            await expect(Remix1.connect(owner).requestDerivative(authors, ADDRESS_ZERO), "Caller is not an RMX contract not reverted")
                .to.be.rejectedWith("INVALID_REMIX_CONTRACT");
        });

        it("Should revert if none of the parent Remix authors have a license", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([addr1.address], [Remix1.address]);
            await expect(deployRemix(args), "Missing author license not reverted")
                .to.be.rejectedWith("MISSING_AUTHOR_LICENSE");
        });
    });

    describe("flag()", () => {
        beforeEach(async () => {
            overrides = { value: utils.parseEther("0.1") };

            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
            await Remix1.connect(addr1).purchaseRMX(overrides);

            args = remixBuildArgs([addr1.address], [Remix1.address]);
            Remix2 = await deployRemix(args, addr1);
            await Remix2.connect(addr2).purchaseRMX(overrides);

            args = remixBuildArgs([addr2.address], [Remix2.address]);
            Remix3 = await deployRemix(args, addr2);
        });

        it("Should revert if sender is not an author of parent Remix", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([addr1.address]);
            Remix2 = await deployRemix(args);

            await expect(Remix2.connect(addr2).flag([Remix1.address]), "Sender is not an author not reverted")
                .to.be.rejectedWith("SENDER_IS_NOT_AN_AUTHOR");
        });

        it("Should revert if the child Remix is not a derivative of the parent Remix", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix2 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix3 = await deployRemix(args);

            await expect(Remix1.connect(owner).flag([Remix2.address, Remix3.address]), "Address is not a child in parents chain not reverted")
                .to.be.rejectedWith("ADDRESS_IS_NOT_A_CHILD_IN_PARENTS_CHAIN");
        });

        it("Should flag from first parent", async () => {
            await Remix3.connect(addr1).flag([Remix3.address, Remix2.address]);

            expect(await Remix3.isFlagged(), "The contract has not been flagged")
                .to.be.true;
        });

        it("Should flag from any parent", async () => {
            await Remix3.connect(owner).flag([Remix3.address, Remix2.address, Remix1.address]);

            expect(await Remix3.isFlagged(), "The contract has not been flagged")
                .to.be.true;
        });

        it("Should register the flagging parent in mapping", async () => {
            await Remix3.connect(addr1).flag([Remix3.address, Remix2.address]);

            expect(await Remix3.flaggingParentsExists(Remix2.address), "The contract has not been flagged")
                .to.be.true;
            expect(await Remix3.getFlaggingParents(), "Returned flagging parents are not correct")
                .to.be.an("array")
                .that.include(Remix2.address);
        });

        it("Should allow multiple flags", async () => {
            await Remix3.connect(addr1).flag([Remix3.address, Remix2.address]);
            expect(await Remix3.isFlagged(), "The contract has not been flagged")
                .to.be.true;

            await Remix3.connect(owner).flag([Remix3.address, Remix2.address, Remix1.address]);
            expect(await Remix3.isFlagged(), "The contract has not been flagged")
                .to.be.true;

            expect(await Remix3.getFlaggingParents(), "Returned flagging parents are not correct")
                .to.be.an("array")
                .that.eql([Remix2.address, Remix1.address]);

            await Remix3.connect(addr1).unflag([Remix3.address, Remix2.address], 0);
            expect(await Remix3.isFlagged(), "The contract should still be flagged")
                .to.be.true;

            await Remix3.connect(owner).unflag([Remix3.address, Remix2.address, Remix1.address], 0);
            expect(await Remix3.isFlagged(), "The contract should not be flagged")
                .to.be.false;
        });

        it("Should emit the Flag signal", async () => {
            await expect(Remix3.connect(addr1).flag([Remix3.address, Remix2.address]), "Did not emit the Flag signal")
                .to.emit(Remix3, "Flagged")
                .withArgs(addr1.address, Remix3.address);
        });

        it("Should only allow an author of a parent to flag", async () => {
            await expect(Remix2.connect(addr2).flag([Remix2.address, Remix1.address]))
                .to.be.rejected;
            expect(await Remix2.isFlagged(), "The contract been flagged when it wasn't supposed to be!")
                .to.be.false;
        });

        it("Should not be transferable", async () => {
            // TODO: Deploy an ERC20 contract
            throw Error("Must be implemented");
        });
    });

    describe("unflag()", () => {
        beforeEach(async () => {
            overrides = { value: utils.parseEther("0.1") };

            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args, owner);
            await Remix1.connect(addr1).purchaseRMX(overrides);

            args = remixBuildArgs([addr1.address], [Remix1.address]);
            Remix2 = await deployRemix(args, addr1);
            await Remix2.connect(addr2).purchaseRMX(overrides);

            args = remixBuildArgs([addr2.address], [Remix2.address]);
            Remix3 = await deployRemix(args);

            await Remix2.connect(owner).flag([Remix2.address, Remix1.address]);
            await Remix3.connect(addr1).flag([Remix3.address, Remix2.address]);
            await Remix3.connect(owner).flag([Remix3.address, Remix2.address, Remix1.address]);
        });

        it("Should revert if the Remix is not flagged", async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix2 = await deployRemix(args);

            await expect(Remix1.connect(owner).unflag([Remix2.address], 0), "Address is not flagged not reverted")
                .to.be.rejectedWith("NOT_FLAGGED");
        });

        it("Should unflag from a direct parent", async () => {
            expect(await Remix2.isFlagged(), "The starting contract has not been flagged")
                .to.be.true;

            await Remix2.connect(owner).unflag([Remix2.address, Remix1.address], 0);

            expect(await Remix2.isFlagged(), "The contract has not been unflagged")
                .to.be.false;
        });

        it("Should unflag from any parent", async () => {
            expect(await Remix3.isFlagged(), "The starting contract has not been flagged")
                .to.be.true;
            expect(await Remix3.getFlaggingParents(), "The parent is not flagging to begin with!")
                .to.be.an("array")
                .that.include(Remix1.address);

            await Remix3.connect(owner).unflag([Remix3.address, Remix2.address, Remix1.address], 1);

            expect(await Remix3.getFlaggingParents(), "The parent has not been removed from the flaggers")
                .to.be.an("array")
                .that.does.not.include(Remix1.address);
        });

        it("Should verify that the parent chain is valid", async () => {
            await expect(Remix3.connect(addr2).unflag([Remix3.address, Remix1.address, Remix2.address], 0), "The parent chain was not verified")
                .to.be.rejected;
        });

        it("Should verify the that the index corresponds to the correct parent", async () => {
            await expect(Remix3.connect(owner).unflag([Remix3.address, Remix2.address, Remix1.address], 0), "The index was not verified")
                .to.be.rejected;
        });

        it("Should allow to be multiply flagged and partially unflagged", async () => {
            await Remix3.connect(addr1).unflag([Remix3.address, Remix2.address], 0);
            expect(await Remix3.isFlagged(), "The contract should still be flagged")
                .to.be.true;

            await Remix3.connect(owner).unflag([Remix3.address, Remix2.address, Remix1.address], 0);
            expect(await Remix3.isFlagged(), "The contract should not be flagged")
                .to.be.false;
        });

        it("Should emit the unflag signal", async () => {
            await expect(Remix2.connect(owner).unflag([Remix2.address, Remix1.address], 0), "Did not emit the UnFlagged signal")
                .to.emit(Remix2, "UnFlagged").withArgs(owner.address, Remix2.address);
        });

        it("Should not allow a non author of a valid parent to unflag", async () => {
            expect(await Remix2.isFlagged(), "The starting contract has not been flagged")
                .to.be.true;
            await expect(Remix2.connect(addr2).unflag([Remix2.address, Remix1.address], 0))
                .to.be.rejected;
        });

        it("Should allow to reflag", async () => {
            await Remix2.connect(owner).unflag([Remix2.address, Remix1.address], 0);
            expect(await Remix2.isFlagged(), "The contract has not been unflagged")
                .to.be.false;

            await Remix2.connect(owner).flag([Remix2.address, Remix1.address]);
            expect(await Remix2.isFlagged(), "The contract has not been flagged")
                .to.be.true;

            await Remix2.connect(owner).unflag([Remix2.address, Remix1.address], 0);
            expect(await Remix2.isFlagged(), "The contract has not been unflagged")
                .to.be.false;
        });
    });

    describe("receive()", () => {
        beforeEach(async () => {
            // Get the ContractFactory and Signers here
            Remix = await ethers.getContractFactory("Remix") as unknown as IsRemix;
            [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        });

        it("Should emit a RoyaltyReceived event", async () => {
            // TODO: Deploy an ERC20 contract
            throw Error("Must be implemented");
        });
    });

    describe("royaltyInfo()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should give the correct royalty info", async () => {
            const value = utils.parseEther("1");
            const royaltyInfo = await Remix1.royaltyInfo(0, value);

            expect(royaltyInfo._receiver, "Receiver address is not correct")
                .to.equal(Remix1.address);
            expect(royaltyInfo._royaltyAmount, "Royalties amount is not correct")
                .to.equal(utils.parseEther("0.1"));
        });

        it("Should require the token to be a collectible", async () => {
            const value = utils.parseEther("1");

            await expect(Remix1.royaltyInfo(1, value), "Should have reverted")
                .to.be.rejected;
        });
    });

    describe("getAuthorsAndSplits()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address, addr1.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should give the correct authors and splits array", async () => {
            const authorsAndSplits = await Remix1.getAuthorsAndSplits();
            const splits = BigNumber.from(5000);

            expect(authorsAndSplits, "Authors and splits are not correct")
                .to.eql([[owner.address, addr1.address], [splits, splits]]);
        });
    });

    describe("getAuthors()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address, addr1.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should give the correct authors array", async () => {
            const authors = await Remix1.getAuthors();

            expect(authors, "Authors are not correct")
                .to.eql([owner.address, addr1.address]);
        });
    });

    describe("getParentsAndSplits()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix2 = await deployRemix(args);

            args = remixBuildArgs([owner.address], [Remix1.address, Remix2.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should give the correct parents and splits array", async () => {
            const parentsAndSplits = await Remix3.getParentsAndSplits();
            const splits = BigNumber.from(3333);

            expect(parentsAndSplits, "Parents and splits are not correct")
                .to.eql([[Remix1.address, Remix2.address], [splits, splits]]);
        });
    });

    describe("getParents()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix2 = await deployRemix(args);

            args = remixBuildArgs([owner.address], [Remix1.address, Remix2.address]);
            Remix3 = await deployRemix(args);
        });

        it("Should give the correct parents array", async () => {
            const parents = await Remix3.getParents();

            expect(parents, "Parents are not correct")
                .to.eql([Remix1.address, Remix2.address]);
        });
    });

    describe("getRoyalties()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);

            args = remixBuildArgs([owner.address]);
            Remix2 = await deployRemix(args);
        });

        it("Should give the correct royalties", async () => {
            throw Error("Must be implemented inside the Remix contract");
        });
    });

    describe("getFlaggingParents()", () => {
        beforeEach(async () => {
            overrides = { value: utils.parseEther("0.1") };

            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
            await Remix1.connect(addr1).purchaseRMX(overrides);

            args = remixBuildArgs([addr1.address], [Remix1.address]);
            Remix2 = await deployRemix(args, addr1);
            await Remix2.connect(addr2).purchaseRMX(overrides);

            args = remixBuildArgs([addr2.address], [Remix2.address]);
            Remix3 = await deployRemix(args, addr2);
        });

        it("Should give no flagging parents if Remix not flagged", async () => {
            const flaggingParents = await Remix3.getFlaggingParents();

            expect(flaggingParents, "Flagging parents should be an empty array")
                .to.be.an("array")
                .to.eql([]);
        });

        it("Should give the correct flagging parents if Remix flagged", async () => {
            await Remix3.connect(addr1).flag([Remix2.address]);

            const flaggingParents = await Remix3.getFlaggingParents();

            expect(flaggingParents, "Flagging parents are not correct")
                .to.be.an("array")
                .to.eql([Remix2.address]);
        });
    });

    describe("isFlagged()", () => {
        beforeEach(async () => {
            overrides = { value: utils.parseEther("0.1") };

            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
            await Remix1.connect(addr1).purchaseRMX(overrides);

            args = remixBuildArgs([addr1.address], [Remix1.address]);
            Remix2 = await deployRemix(args, addr1);
            await Remix2.connect(addr2).purchaseRMX(overrides);

            args = remixBuildArgs([addr2.address], [Remix2.address]);
            Remix3 = await deployRemix(args, addr2);
        });

        it("Should return false if not flagged", async () => {
            const isFlagged = await Remix3.isFlagged();

            expect(isFlagged, "Remix should not be flagged")
                .to.be.false;
        });

        it("Should return true if flagged", async () => {
            await Remix3.connect(addr1).flag([Remix2.address]);

            const isFlagged = await Remix3.isFlagged();

            expect(isFlagged, "Remix should be flagged")
                .to.be.true;
        });
    });

    describe("remixLicenseActive()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should be a valid Remix license if this is the author", async () => {
            const license = await Remix1.remixLicenseActive(owner.address);

            expect(license === true, "Authors does not have the Remix license!")
                .to.be.true;
        });

        it("Should be an invalid Remix license if this is not the author", async () => {
            const license = await Remix1.remixLicenseActive(addr1.address);

            expect(license === false, "Stranger has the Remix license without the RMX or being author!")
                .to.be.true;
        });

        it("Should be a valid Remix license if the address owns an RMX token", async () => {
            overrides = { value: utils.parseEther("0.1") };
            await Remix1.connect(addr1).purchaseRMX(overrides);
            const license = await Remix1.remixLicenseActive(addr1.address);

            expect(license == true, "Owner of RMX does not have the Remix license!")
                .to.be.true;
        });
    });

    describe("usageLicenseActive()", () => {
        beforeEach(async () => {
            args = remixBuildArgs([owner.address]);
            Remix1 = await deployRemix(args);
        });

        it("Should be a valid license if the holder bought the Usage right", async () => {
            overrides = { value: utils.parseEther("0.1") };
            await Remix1.connect(addr1).purchaseUsage(overrides);

            const license = await Remix1.usageLicenseActive(addr1.address);

            expect(license === true, "Usage license is not active after buying the usage right!")
                .to.be.true;
        });

        it("Should be an invalid license if the holder did not bought the Usage right", async () => {
            const license = await Remix1.usageLicenseActive(addr1.address);

            expect(license === false, "Usage license is active without buying the usage right!")
                .to.be.true;
        });
    });
});