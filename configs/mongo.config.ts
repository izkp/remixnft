import { MongoClientOptions, ServerApiVersion } from "mongodb";


export const mongoConfig: MongoClientOptions = {
    serverApi: ServerApiVersion.v1
};