import { ethers, getNamedAccounts } from "hardhat";
import "hardhat-deploy";
import { sys } from "typescript";

import { GENERAL } from "general.config";
import logger from "utils/logger";


const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

/**
 * Main function.
 */
async function main(): Promise<void> {
    const toAddress = GENERAL.mintingAddress;

    logger.info(`Minting to ${toAddress}..\n`);

    const { deployer } = await getNamedAccounts();
    const yourCollectible = await ethers.getContractAt("YourCollectible", deployer);

    await yourCollectible.mint(toAddress, 0, 4, [], { gasLimit: 400000 });
    await yourCollectible.mint(toAddress, 1, 10, [], { gasLimit: 400000 });
    await yourCollectible.mint(toAddress, 2, 2, [], { gasLimit: 400000 });
    await yourCollectible.mint(toAddress, 3, 5, [], { gasLimit: 400000 });
    await yourCollectible.mint(toAddress, 4, 6, [], { gasLimit: 400000 });
    await yourCollectible.mint(toAddress, 5, 1, [], { gasLimit: 400000 });

    await sleep(GENERAL.xdaiSleepingTime);
}

main()
    .then(() => sys.exit(0))
    .catch((error) => {
        logger.error(error);
        sys.exit(1);
    });