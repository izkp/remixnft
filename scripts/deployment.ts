import fs from "fs";

import { mongoClient, mongoDB } from "helpers/mongo";
import NsContracts from "types/contracts";
import { sys } from "typescript";

import { GENERAL, PATHS } from "general.config";
import logger from "utils/logger";


/**
 * Creates or updates a record containing the deployed factory addresses.
 */
async function deploymentsRecord(): Promise<void> {
    const hardhatContractsPath = `${PATHS.artifactsInput}/hardhat_contracts.json`;

    if (fs.existsSync(hardhatContractsPath)) {
        const contractsData: NsContracts.IsHardhatContracts = JSON.parse(
            fs.readFileSync(hardhatContractsPath).toString()
        );

        for (const chainId of Object.keys(contractsData)) {
            const networkData = contractsData[chainId];

            // Iterate through the networks,
            // Used as a fallback because normally, only one network
            // should be present per chainId in our case
            for (const network of Object.keys(networkData)) {
                const contracts = networkData[network].contracts;
                const networkName = networkData[network].name;

                for (const contract of Object.keys(contracts)) {
                    const contractAddress = contracts[contract].address;

                    if (!(networkName in GENERAL.ignoredNetworks)) {
                        if (contract in GENERAL.recordedContracts) {
                            const collectionName = GENERAL.recordedContracts[contract];
                            const collection = mongoDB.collection(collectionName);

                            // Note that this object only contains general information,
                            // because it is used for any contract that is deployed (not just the factory)
                            const object = { address: contractAddress, network: networkName };
                            await collection.updateOne(object, { $set: object }, { upsert: true });

                            logger.info(`Updated ${contract} address in ${collectionName} collection..`);
                        } else {
                            logger.warn(`${contract} is not in the recordedContracts list, skipping..`);
                        }
                    } else {
                        logger.warn(`${networkName} is in the ignoredNetworks list, skipping..`);
                    }
                }
            }
        }
    } else {
        logger.warn("No hardhat_contracts.json file found, skipping deployments record..");
    }

    await mongoClient.close();

    return Promise.resolve();
}


/**
 * Copy the artifacts from the hardhat artifacts directory and copy them to the PATHS.outputs.
 */
function copyArtifacts(): void {
    if (!fs.existsSync(PATHS.artifactsInput)) {
        logger.error("No artifacts found, please use 'hardhat compile' to generate these files");
        sys.exit(1);
    }

    // Get all the names of the dirs/files inside the artifacts/contracts directory
    const names = fs.readdirSync(PATHS.artifactsInput, { withFileTypes: false });
    const recursivePaths: string[] = [];
    const allPaths: string[] = [];

    // Add all the paths (relative to artifacts Input) to the recursivePaths array
    for (const name of names) {
        const path = `${PATHS.artifactsInput}/${name}`;

        // Ignore root files (such as hardhat_contracts.json)
        if (fs.lstatSync(path).isDirectory()) {
            recursivePaths.push(path);
        }
    }

    // Recursive way to recover all artifact paths
    while (recursivePaths.length > 0) {
        for (const recursivePath of recursivePaths) {
            // If the dir includes ".sol", it means it's a contract directory (contains the .json file)
            if (recursivePath.includes(".sol")) {
                allPaths.push(recursivePath);

                // If not, it means it's a subdirectory, so we need to add all the files inside it to the recursivePaths array
            } else {
                const names = fs.readdirSync(recursivePath, { withFileTypes: false });

                for (const name of names) {
                    const path = `${recursivePath}/${name}`;
                    recursivePaths.push(path);
                }
            }

            // Remove the original path from the array
            recursivePaths.splice(recursivePaths.indexOf(recursivePath), 1);
        }
    }

    // Get and copy the artifacts
    for (const path of allPaths) {
        const contractName = path.substring(path.lastIndexOf("/") + 1).replace(".sol", "");
        const inputFilePath = `${path}/${contractName}.json`;

        if (fs.existsSync(inputFilePath)) {
            for (const outputPath of PATHS.outputs) {
                // Create the missing directories if they don't exist
                if (!fs.existsSync(outputPath)) {
                    fs.mkdir(outputPath, { recursive: true }, (err) => {
                        if (err) {
                            logger.error(`Problem while creating the output directory '${outputPath}'`);
                        }
                    });
                }

                // Copy the files to the output directory
                fs.copyFile(inputFilePath, `${outputPath}/${contractName}.json`,
                    (err) => {
                        if (err) throw err;
                        logger.info(`Copied '${contractName}.sol' artifact to ${outputPath}`);
                    });
            }
        } else {
            logger.error(`Path for '${contractName}' is incorrect..`);
        }
    }
}

deploymentsRecord()
    .then(() => {
        copyArtifacts();
    })
    .catch((error) => {
        logger.error(error);
        sys.exit(1);
    });