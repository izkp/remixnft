import { exec } from "child_process";

import watch from "node-watch";

import { PATHS } from "general.config";
import logger from "utils/logger";


/**
 * Simple function to watch for changes in the contracts directory
 * and run the copy script when a change is detected.
 */
function run (): void {
    logger.info("Compiling & Deploying contracts...");
    exec("yarn deploy && ts-node scripts/deployment.ts",
        (error, stdout, stderr) => {
            logger.info(stdout);

            if (error) logger.error(error);
            if (stderr) logger.error(stderr);
        }
    );
}

logger.info("Watching contracts for changes...");

watch(PATHS.artifactsInput, { recursive: true }, (evt, name) => {
    logger.warn(`Change detected in ${name}...`);
    run();
});
