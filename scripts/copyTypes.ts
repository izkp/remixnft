import fs from "fs";

import * as fse from "fs-extra";
import { sys } from "typescript";

import { PATHS } from "general.config";
import logger from "utils/logger";


/**
 * Copies the typechain types to the output paths.
 */
function copyTypes() {
    if (!fs.existsSync(PATHS.typesInput)) {
        logger.error("No typechain types found, please use 'hardhat compile' to generate these files");
        sys.exit(1);
    }

    for (const outputPath of PATHS.outputs) {
        const finalPath = `${outputPath}/types`;

        // Create the missing directories if they don't exist
        if (!fs.existsSync(finalPath)) {
            fs.mkdir(finalPath, { recursive: true }, (err) => {
                if (err) {
                    logger.error(`Problem while creating the output directory '${finalPath}'`);
                }
            });
        }

        try {
            fse.copySync(PATHS.typesInput, `${finalPath}`);
            logger.info(`Copied typechain types to ${finalPath}`);
        } catch (err) {
            logger.error(err);
            sys.exit(1);
        }
    }
}

copyTypes();