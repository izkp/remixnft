declare namespace NsContracts {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * General configuration for the contracts.
     */
    interface IsConfigGENERAL {
        verbose: boolean;

        xdaiSleepingTime: number;
        mintingAddress: string;

        recordedContracts: {
            [key: string]: string;
        };

        ignoredNetworks: string[];
    }

    /**
     * Deploy examples: Metadata interface.
     */
    interface IsMetadata {
        result: string;
        collectible: string;
        rmx: string;
        file: string;
        name: string;
        description: string;
        cid: CID;
    }

    /**
     * Contains an object of all the deployed factory addresses,
     * formatted as RemixFactory_UNIX_TIMESTAMP.
    */
    interface IsDeployments {
        [key: string]: {
            [key: string]: string;
        };
    }

    /**
     * Emitted hardhat_contracts.json file internal contracts type.
     */
    interface IsHardhatContractsABIs {
        [key: string]: {
            address: string;
            abi: typeof RemixJSON.abi | typeof RemixFactoryJSON.abi;
        };
    }

    /**
     * Emitted hardhat_contracts.json file internal network type.
     */
    interface IsHardhatContractsNetwork {
        [key: string]: {
            name: string;
            chainId: number;
            contracts: IsHardhatContractsABIs;
        };
    }

    /**
     * Emitted hardhat_contracts.json file type.
     */
    interface IsHardhatContracts {
        [key: string]: IsHardhatContractsNetwork;
    }

    /**
     * Hardhat network default interface.
     */
    interface IsNetwork {
        url?: string;
        gasPrice?: number;
        chainId?: number;
        accounts?: {
            mnemonic: string | undefined;
        };
        companionNetworks?: {
            l1?: string;
            l2?: string;
        };
        ovm?: boolean;
        forking?: {
            url: string;
            blockNumber?: number;
        };
    }

    /**
     * Transaction receipt type.
     */
    interface IsTx {
        from?: string;
        to: string | undefined;
        value: string;
        nonce?: number;
        gasPrice?: BigNumber;
        gasLimit?: BigNumber;
        chainId?: number;
        data?: string;
    }

    /**
     * A list of Hardhat networks.
     */
    interface IsNetworks {
        [key: string]: IsNetwork;
    }
}


export default NsContracts;