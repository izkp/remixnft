import { ContractTransaction } from "ethers";


declare namespace NsRemix {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * Payable functions overrides.
     */
    interface IsPayableOverrides {
        value: number | BigNumber;
    }

    /**
     * royaltyInfo() output interface.
     */
    interface IsRoyaltyInfoOutput {
        _receiver: string;
        _royaltyAmount: BigNumber | number;
    }

    /**
     * getAuthorsAndSplits() output interface.
     */
    interface IsAuthorsAndSplitsOutput {
        _authors: string[];
        _authorSplits: number[] | BigNumber[];
    }

    /**
     * getParentsAndSplits() output interface.
     */
    interface IsParentsAndSplitsOutput {
        _parents: string[];
        _parentSplits: number[] | BigNumber[];
    }

    /**
     * getRoyalties() output interface.
     */
    interface IsGetRoyaltiesOutput {
        _addresses: string[];
        _royalties: number[] | BigNumber[];
    }

    /**
     * Extends the ContractTransaction interface for Remix
     * to include all the data we need to test.
    */
    interface IsWrapper extends ContractTransaction {
        address: string;
        connect: (signer: Signer | Provider | string) => IsWrapper;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        queryFilter: (event: string) => Promise<any>;
        balanceOf: (address: string, id: number | BigNumber) => Promise<BigNumber | number>;
        owner: () => Promise<string>;

        // Public variables
        factory: () => Promise<string>;
        currentCollectibleOwner: () => Promise<string>;
        currentRMXOwner: () => Promise<string>;
        authors: (index: number) => Promise<string[]>;
        parents: (index: number) => Promise<string[]>;
        flaggingParents: (index: number) => Promise<string[]>;
        splitAddresses: (index: number) => Promise<string[]>;
        collectiblePrice: () => Promise<BigNumber | number>;
        usagePrice: () => Promise<BigNumber | number>;
        minPurchasePrice: () => Promise<BigNumber | number>;
        royalties: () => Promise<BigNumber | number>;
        increasePoints: () => Promise<BigNumber | number>;
        countdownTime: () => Promise<BigNumber | number>;
        factoryCut: () => Promise<BigNumber | number>;
        authorSplits: (index: number) => Promise<number[] | BigNumber[]>;
        parentSplits: (index: number) => Promise<number[] | BigNumber[]>;
        hasBeenPurchased: () => Promise<boolean>;
        flaggingParentsExists: (address: string) => Promise<boolean>;
        splits: (address: string) => Promise<number[] | BigNumber[]>;

        // List of Remix functions
        initialize: (...args: IsContractBuildArgs) => Promise<ContractTransaction>;
        purchaseRMX: (overrides: IsPayableOverrides) => Promise<void>;
        purchaseCollectible: (overrides: IsPayableOverrides) => Promise<void>;
        purchaseUsage: (overrides: IsPayableOverrides) => Promise<void>;
        harvestRoyalties: (_tokenAddress: string) => Promise<void>;
        requestDerivative: (_authors: string[], _dst: string) => Promise<boolean>;
        flag: (_parents: string[]) => Promise<void>;
        unflag: (_parents: string[], _index: number | BigNumber) => Promise<void>;
        receive: (overrides: IsPayableOverrides) => Promise<void>;
        royaltyInfo: (_tokenId: number | BigNumber, _salePrice: number | BigNumber) => Promise<IsRoyaltyInfoOutput>;
        getAuthorsAndSplits: () => Promise<IsAuthorsAndSplitsOutput>;
        getAuthors: () => Promise<string[]>;
        getParentsAndSplits: () => Promise<IsParentsAndSplitsOutput>;
        getParents: () => Promise<string[]>;
        getRoyalties: (_tokenAddresses: string) => Promise<IsGetRoyaltiesOutput>;
        getFlaggingParents: () => Promise<string[]>;
        isFlagged: () => Promise<boolean>;
        remixLicenseActive: (_holder: string) => Promise<boolean>;
        usageLicenseActive: (_holder: string) => Promise<boolean>;
        supportsInterface: (_interfaceId: string) => Promise<boolean>;
    }
}


export default NsRemix;