import { ContractTransaction } from "ethers";


declare namespace NsRemixFactory {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * Payable functions overrides.
     */
    interface IsPayableOverrides {
        value: number | BigNumber;
    }

    /**
     * Extends the ContractTransaction interface for RemixFactory
     * to include all the data we need to test.
     */
    interface IsWrapper extends ContractTransaction {
        address: string;
        connect: (signer: Signer | Provider | string) => IsWrapper;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        queryFilter: (event: string) => Promise<any>;
        balanceOf: (address: string, id: number | BigNumber) => Promise<BigNumber | number>;
        owner: () => Promise<string>;

        // Roles
        addOrRevokeModerator: (account: string) => Promise<void>;
        hasModeratorRole: (account: string) => Promise<boolean>;
        hasRole: (role: string, account: string) => Promise<boolean>;
        DEFAULT_ADMIN_ROLE: () => Promise<string>;
        MODERATOR_ROLE: () => Promise<string>;

        // Public variables
        count: () => Promise<BigNumber | number>;

        // List of Remix Factory functions
        deploy: (...args: IsContractBuildArgs) => Promise<ContractTransaction>;
        registerRemix: (_remixProxyAddress: string) => Promise<void>;
        upgradeRemixImplementation: (_newRemixImplementation: string) => Promise<void>;
        harvestMany: (_remixes: string[], _tokenAddress: string) => Promise<void>;
        harvestFactory: (_tokenAddress: string, _to: string) => Promise<void>;
        updateFactoryCut: (_newCut: number | BigNumber) => Promise<void>;
        flag: (_remixProxyAddress: string) => Promise<void>;
        unflag: (_remixProxyAddress: string, _index: number | BigNumber) => Promise<void>;
        getRemixImplementation: () => Promise<string>;
        getRemixByAuthor: (_author: string) => Promise<string[]>;
        supportsInterface: (_interfaceId: string) => Promise<boolean>;
    }
}


export default NsRemixFactory;