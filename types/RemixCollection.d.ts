import { BigNumber, ContractTransaction } from "ethers";


declare namespace NsRemixCollection {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * Payable functions overrides.
     */
    interface IsPayableOverrides {
        value: number | BigNumber;
    }

    /**
     * getWinningRemixes() output interface.
     */
    interface IsWinningRemixesOutput {
        _remixProxyAddress: string;
        _selectionNumber: BigNumber | number;
    }

    /**
     * Extends the ContractTransaction interface for Remix Collection
     * to include all the data we need to test.
    */
    interface IsWrapper extends ContractTransaction {
        address: string;
        connect: (signer: Signer | Provider | string) => IsWrapper;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        queryFilter: (event: string) => Promise<any>;
        balanceOf: (address: string, id: number | BigNumber) => Promise<BigNumber | number>;
        owner: () => Promise<string>;

        // Public variables
        factory: () => Promise<string>;
        allSubmissions: (index: number) => Promise<string[]>;
        allSelections: (index: number) => Promise<string[]>;
        startingRemixes: (index: number) => Promise<string[]>;
        metadata: () => Promise<string>;
        submissionPrice: () => Promise<BigNumber | number>;
        totalSelections: () => Promise<BigNumber | number>;
        totalValueHeld: () => Promise<BigNumber | number>;
        submissions: (address: string) => Promise<BigNumber | number>;
        selection: (slot: number | BigNumber) => Promise<string>;
        status: () => Promise<BigNumber | number>;

        // List of Remix Collection functions
        initialize: (...args: IsContractBuildArgs) => Promise<ContractTransaction>;
        open: () => Promise<void>;
        close: () => Promise<void>;
        submit: (_remixProxyAddress: string, _selectionNumber: BigNumber | number, overrides: IsPayableOverrides) => Promise<void>;
        select: (_remixProxyAddress: string) => Promise<void>;
        finalize: (_tokenAddress: string) => Promise<void>;
        getWinningRemixes: () => Promise<IsWinningRemixesOutput[]>;
        getSubmissionsCount: () => Promise<BigNumber | number>;
        getSelectionsCount: () => Promise<BigNumber | number>;
        getStatus: () => Promise<BigNumber | number>;
        isSubmitted: (_remixProxyAddress: string) => Promise<boolean>;
    }
}


export default NsRemixCollection;