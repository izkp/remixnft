import { ContractTransaction } from "ethers";


declare namespace NsRemixCollectionFactory {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * Extends the ContractTransaction interface for RemixCollectionFactory
     * to include all the data we need to test.
     */
    interface IsWrapper extends ContractTransaction {
        address: string;
        connect: (signer: Signer | Provider | string) => IsWrapper;

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        queryFilter: (event: string) => Promise<any>;
        owner: () => Promise<string>;

        // Roles
        addOrRevokeModerator: (account: string) => Promise<void>;
        hasModeratorRole: (account: string) => Promise<boolean>;
        hasRole: (role: string, account: string) => Promise<boolean>;
        DEFAULT_ADMIN_ROLE: () => Promise<string>;
        MODERATOR_ROLE: () => Promise<string>;

        // Public variables
        count: () => Promise<BigNumber | number>;

        // List of Remix Collection Factory functions
        deploy: (...args: IsContractBuildArgs) => Promise<ContractTransaction>;
        registerRemixCollection: (_remixCollectionProxyAddress: string) => Promise<void>;
        upgradeRemixCollectionImplementation: (_newRemixCollectionImplementation: string) => Promise<void>;
        getRemixCollectionImplementation: () => Promise<string>;
        getRemixCollectionByAuthor: (_author: string) => Promise<string[]>;
    }
}


export default NsRemixCollectionFactory;