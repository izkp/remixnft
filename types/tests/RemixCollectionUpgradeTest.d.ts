import { ContractTransaction } from "ethers";

import NsRemixCollection from "types/RemixCollection";


declare namespace NsRemixCollectionUpgradeTest {
    /**
     * Note about numbers coming from the blockchain:
     * They should be represented as numbers AND BigNumbers.
     * -> number | BigNumber
     */

    /**
     * Args for a general contract deployment/initialization.
     */
    type IsContractBuildArgs = (string | number | string[] | number[] | BigNumber | undefined)[];

    /**
     * Extends the ContractTransaction interface for RemixUpgradeTest
     * to include all the data we need to test.
     *
     * Note: Also extends the IsWrapper interface for the RemixCollection contract
     * so only new upgrade test methods need to be added here.
     */
    interface IsWrapper extends ContractTransaction, NsRemixCollection.IsWrapper {
        testUpgrade: () => Promise<string>;
    }
}


export default NsRemixCollectionUpgradeTest;