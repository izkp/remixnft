import { utils } from "ethers";

import NsRemix from "types/Remix";
import NsRemixCollection from "types/RemixCollection";


// CONSTANTS
export const ADDRESS_ZERO = "0x0000000000000000000000000000000000000000";

/**
 * Test Remix contract with these build args.
 * @param authors The authors of the contract.
 * @param parents The parents of the contract.
 * @param tokenUri The token URI.
 * @returns The args used for the contract.
 */
export function remixBuildArgs(
    authors: string[],
    parents: string[] = [],
    tokenUri: string | undefined = undefined
): NsRemix.IsContractBuildArgs {
    let authorSplits: number[] = [1000];
    let parentSplits: number[] = [];

    if (authors.length > 0) {
        const split: number = 10000 / (authors.length + parents.length);

        authorSplits = authors.map(() => split);
        parentSplits = parents.map(() => split);

        if (split * (authors.length + parents.length) !== 10000) {
            authorSplits[0] += 10000 - (split * (authors.length + parents.length));
        }

        // Round the values
        authorSplits = authorSplits.map((split) => Math.round(split));
        parentSplits = parentSplits.map((split) => Math.round(split));

        // Make sure the total is equal to 10k, if not, add the difference to the first value
        // The reason for this is that the values are rounded, so the total may not be 10k
        // In case of 2 parents for example where the value would be 3334
        const authorTotal = authorSplits.reduce((a, b) => a + b, 0);
        const parentTotal = parentSplits.reduce((a, b) => a + b, 0);

        if (authorTotal + parentTotal !== 10000) {
            authorSplits[0] += 10000 - (authorTotal + parentTotal);
        }
    }

    if (!tokenUri) {
        tokenUri = process.env.TOKEN_URI;
    }

    const args: NsRemix.IsContractBuildArgs = [
        tokenUri,                    // string memory uri_,
        authors,                     // address[] memory _authors,
        authorSplits,                // uint256[] memory _authorSplits,
        parents,                     // address[] memory _parents,
        parentSplits,                // uint256[] memory _parentSplits,
        utils.parseEther("0.01"),    // uint256 _startingPrice,
        100,                         // uint256 _increasePoints,
        utils.parseEther("0.1"),     // uint256 _collectiblePrice,
        utils.parseEther("0.05"),    // uint256 _usagePrice,
        10000,                       // uint256 _rmxCountdown,
        1000                         // uint256 _royalties
    ];

    return args;
}

/**
 * Test RemixCollection contract with these build args.
 * @returns The args used for the contract.
 */
export function remixCollectionBuildArgs(): NsRemixCollection.IsContractBuildArgs {
    const metadataURI = "http://myMetdata.com/metadata.json";

    const args: NsRemixCollection.IsContractBuildArgs = [
        metadataURI,                 // string memory _metadata,
        utils.parseEther("0.01"),    // uint256 _submissionPrice,
        3,                           // uint256 _totalSelections,
        []                           // address[] memory _startingRemixes
    ];

    return args;
}