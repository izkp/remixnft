import { mongoConfig } from "configs/mongo.config";
import dotenv from "dotenv";
import { MongoClient } from "mongodb";


dotenv.config();

const URI = process.env.MONGO_URI as string;
const DB = process.env.MONGO_DB as string;

export const mongoClient = new MongoClient(URI, mongoConfig);
export const mongoDB = mongoClient.db(DB);