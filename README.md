# Remix NFT (Package)
The Remix NFT package is used to deploy smart contracts using Hardhat,
and to send the respective contract ABIs to the other packages of the app.

**IMPORTANT NOTE:** Please use Node v16.20.0 LTS because Hardhat doesn't support Node v17/18 yet (causes an error while trying to connect to a local chain).

Directories
-----------
- The `contracts/` directory is used to store all the Solidity contracts that will be compiled/deployed on chain.
- The `deploy/` directory is used to store all the contract deploying functions (different steps & setup to deploy each contract).
- The `scripts/` directory is used to store all the general scripts
used by the commands to copy the ABIs to the right, to watch or mint, etc..
- The `tests/` directory allows to write tests using [mocha](https://github.com/mochajs/mocha).
- The `types/` directory is used to store the custom interfaces for the contracts.

Settings
--------
- The `general.config.ts` file is used to store the general settings for the package.
- The `networks.config.ts` file is used to store the network settings for the package.
- The `hardhat.config.ts` file is used to store the Hardhat settings for the package.

Inside of the `general.config.ts` file, you can find fields that are used to configure the `saveAndCopyDeployment` & the `copyContractTypes` commands.

These fields can be described as follows:
- `GENERAL`:
    - `xdaiSleepingTime`: The time to wait between each transaction when deploying on xDai.
    - `verbose`: A boolean that indicates if the Winston logger should be verbose or not.
    - `mintingAddress`: The address to mint assets to (used by the `mint` command).
    - `recordedContracts`: An array containing the contracts that should be recorded inside of the MongoDB.
    - `ignoredNetworks`: An array containing the networks that should be ignored when recording the contracts.
- `PATHS`:
    - `typesInput`: The path to the directory where the contract types are stored.
    - `artifactsInput`: The path to the directory where the contract artifacts are stored.
    - `outputs`: An array containing the paths where the contract types & artifacts should be copied to (other packages).

Settings with .env
------------------
You can check the `.env.sample` file to get an idea of how you can configure it.

The field that will be changed the most is the `HARDHAT_NETWORK` one,
you can find a list of all the configured networks inside of the `networks.config.ts` file.

Main Commands
-------------
- The `chain` command is used to run a local hardhat chain.
- The `test` command runs the test files inside of the `tests/` directory.
- The `compile` command is used to compile the contracts
- The `deploy` command deploys the contracts and export them into a JSON file.
- The `redeploy` command forces to deploy the contracts again even if the ABI is the same.
- The `deployExamples` command deploys the example Remix contracts with a bunch of assets.
- The `watch` command watches the contracts and recompile them when a change is detected (also runs the
    deployment script to copy artifacts & types to other packages).
- The `mint` command runs the `mint` script which allows to mint assets to a specific address (inside `general.config.ts`, `mintingAddress` field).
- The `etherscan-verify-*` command allows to verify any contract on (replace the star with any of these values):
    - mainnet
    - goerli
    - polygon
    - mumbai

### Copy commands:
These commands are used to copy the ABIs/types to the other packages.
- The `saveAndCopyDeployment` command runs the `deployment` script which creates a record
    of all the deployed contracts inside of the MongoDB (except for ignored networks) and copies the ABIs to other packages.
- The `copyContractTypes` command copies the generated TS types for the contracts and send to other packages.

Note that these commands are automatically run after the `deploy` & the `redeploy` commands.

### Other Commands (tasks):
- The `wallet` command creates a PK link with a random address.
- The `fundedWallet` command creates a PK link with a random address and funds it with 100 ETH.
    - With the `--amount` option, you can specify the amount of ETH to send to the wallet (defaults to 0.01 ETH).
    - With the `url` option to specify the URL to add PK to (defaults to `http://localhost:3000`).
- The `generate` command creates a mnemonic for builder deploys.
- The `mineContractAddress` command looks for a deployer account that will give leading zeros
    - With the `--searchFor` parameter (required).
- The `account` command allows to get the balance information for the deployment account.
- The `accounts` command prints the list of accounts.
- The `blockNumber` command prints the current block number.
- The `balance` command prints the balance of an account
    - With the `--account` positional parameter (required), you specify the account to get the balance from.
- The `send` command allows to send ETH to an account:
    - With the `--from` parameter (required), you specify the account to send ETH from.
    - With the `--to` parameter (required), you specify the account to send ETH to.
    - With the `--amount` parameter (required), you specify the amount of ETH to send.
    - With the `--data` parameter, you specify the data to send.
    - With the `--gasPrice` parameter, you specify the gas price to use.
    - With the `--gasLimit` parameter, you specify the gas limit to use.

Interfaces
----------
Mainly for the tests, you can find the interfaces for the contracts inside of the `types/` directory.
These types are used for the proxies, as we can deploy with no args, the generated types don't like
that, so we have to create our own types.

A contract interface file should be named as the contract name, with the `.d.ts` extension.

Tests
-----
A test file should be created for each contract, and the file name should be the same as the contract name,
with the `.test.ts` extension.

Test contracts for the upgrades can be found inside the `contracts/tests/` directory.

Note that, instead of using `reverted` & `revertedWith`, we use `rejected` & `rejectedWith` to check for errors.

Concerning the good writing of the tests, each test should only contain one `expect` statement.
Each function, require & event should be tested in a separate test.

Contract formatting standard
----------------------------
A standard for the contract formatting is used to make it easier to read and understand.

Notes:
- **IMPORTANT:** A public function with ETH & ERC20 cannot and should not be written as a double declared function,
it should be written as a single function with a _tokenAddress == address(0) if ETH is used
and _tokenAddress != address(0) if ERC20 is used, firstly because it's easier to read,
secondly because ABIs are not compatible with public double declared function.
So it's still possible to use them for internal functions, but not for public ones.
```solidity
// INVALID
function harvest() public onlyOwner {...}
function harvest(address _tokenAddress) public onlyOwner {...}

// VALID
function harvest(address _tokenAddress) public onlyOwner {
    if (_tokenAddress == address(0)) {
        // ETH
    } else {
        // ERC20
    }
}
```

- To prevent name clashes, always use the underscore prefix for local/parameter variables and return names.
- All the return values should be named.
- All the functions should be documented using the [NatSpec](https://docs.soliditylang.org/en/v0.8.0/natspec-format.html) format.
- `returns ()` instead of `returns()`.
- About `require()` and its error code format, use uppercase and underscores
  such as `require(_authors.length > 0, "MISSING_AUTHORS");`.
- Structs can be used to group related variables together.
- Array variables should be below normal variables inside variable lists at the beginning of the contract.
- In the case of deploy arguments as a struct, the struct should be declared inside the interface.
- All the contracts should use `ERC165` to check if the contract supports an interface.
- And so, all contracts should implement an interface.

```solidity
// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;


/**
 * Using OpenZeppelin's XXXXXXXXX.sol
 * (Link to the contract inside the GitHub repository)
 *
 * Using __
 */

import "hardhat/console.sol";
import "@openzeppelin/contracts/__";


/// @title Interface for (contract name / standard)
interface IContract {
    /// @notice __
    /// @param __
    /// @return __
    function __() external returns (__);
}

/// @title (Contract name)
contract Contract is IContract {
    // Implementations
    __

    // Roles (if any, otherwise remove this comment)
    __

    // Structs
    __

    // Addresses
    __                                   // STANDARD_NOTE: Comments should be aligned

    // Strings
    __

    // Uint8s
    __

    // Uint256s
    __

    // Booleans
    __

    // Mappings
    __

    // Enums
    __

    // Other types (replace this comment with the type name)
    __

    // Role events (if any, otherwise remove this comment)
    __

    // Events
    __

    // Modifiers
    __


    // STANDARD_NOTE: In the case of an empty constructor, add that comment below,
    // STANDARD_NOTE: and set the constructor in one line.
    // Empty constructor -> use initialize() instead
    constructor() {}


    // STANDARD_NOTE: These functions should be the main functions such as public,
    // STANDARD_NOTE: public payable, external payable, etc..
    /// @dev __
    /// @param __
    function __() public (__) {

    }

    __


    /***************************************
        External & public view interfaces
    ****************************************/

    // STANDARD_NOTE: These functions should be the external & public view functions
    // STANDARD_NOTE: such as external view, public view, etc..
    /// @dev __
    /// @param __
    /// @return __
    function __() external view returns (__) {

    }


    /****************************
        Hooks & Internal utils
    *****************************/

    // STANDARD_NOTE: These functions should be the internal functions
    // STANDARD_NOTE: such as internal, internal view, etc..
    /// @dev __
    /// @param __
    function __() internal (__) {

    }


    /*************************
        Admin & Staff utils
    **************************/

    // STANDARD_NOTE: These functions should be the admin & staff functions
    // STANDARD_NOTE: such as addOrRevokeAdmin, hasAdminRole, etc..
}
```
